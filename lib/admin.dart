import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'item/create.dart';
import 'item/item.dart';

/*

        This is the admin page of the app,
        This part is only shown if the user is an admin.
        If you are an admin, you will see a button in the app.
        And if you click that button you will see this page

 */

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  Map<dynamic, dynamic> dataListings;
  Map<dynamic, dynamic> dataUsers;
  Map<dynamic, dynamic> dataChats;
  var reported;

  @override
  void initState() {
    super.initState();
    _loadData();
    //_createAdminListing();
  }

  void _loadData() async {
    QuerySnapshot qs =
        await Firestore.instance.collection('listings').getDocuments();
    QuerySnapshot users =
        await Firestore.instance.collection('users').getDocuments();
    QuerySnapshot chats =
        await Firestore.instance.collection('inbox').getDocuments();

    dataListings = {
      "numberOfListings": qs.documents.length,
    };

    dataUsers = {
      "numberOfUsers": users.documents.length,
    };

    dataChats = {
      "numberOfChats": chats.documents.length,
    };

    dataListings["reportedDocs"] = [];

    qs.documents.toList().forEach((DocumentSnapshot v) {
      if (v.data["reported"] == true) {
        dataListings["reportedDocs"].add(v);
      }
    });

    setState(() {});
  }
/*
  void _createAdminListing(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateAdminListing()));
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Warmie Admin"),
        backgroundColor: Color(0xFF58A9B1),
      ),
      body: dataListings != null
          ? ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),
                // ---------------- NUMBER OF ITEMS ON SALE -------------------
                new Container(
                  margin: const EdgeInsets.all(10.0),
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black.withOpacity(0.5)),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Text(
                    dataListings["numberOfListings"].toString() +
                        " Items on Sale",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.5)),
                    textAlign: TextAlign.center,
                  ),
                ),
                // ---------------- NUMBER OF USERS REGISTERED ----------------
                new Container(
                  margin: const EdgeInsets.all(10.0),
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black.withOpacity(0.5)),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Text(
                    dataUsers["numberOfUsers"].toString() + " Registered Users",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.5)),
                    textAlign: TextAlign.center,
                  ),
                ),
                // ---------------- NUMBER OF CHATS -----------------
                new Container(
                  margin: const EdgeInsets.all(10.0),
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black.withOpacity(0.5)),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Text(
                    dataChats["numberOfChats"].toString() + " Active Chats",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.5)),
                    textAlign: TextAlign.center,
                  ),
                ),

                //------------------- BUTTON TO SET DATA TO ALL THE USERS OF THE APP ---------------
                // -------------------------------------------------------------------------------------------------------------------------------
    /*
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),
                GestureDetector(
                  onTap: () async {
                    QuerySnapshot qr = await Firestore.instance
                        .collection('users')
                        .getDocuments();
                    qr.documents.forEach((doc) => Firestore.instance
                            .collection('users')
                            .document(doc.documentID)
                            .setData({
                          "userSiteUrl": "Your Website..",
                          "prof": "no",
                          "call": "Your Phone Number..",
                          "companyDescription": "Describe Your Company..",
                          "address": "Your Address..",
                          "companySlogan": "Your Company Slogan..",
                          "numberOfEmployees": "0",
                          "yearsOfExperience": "0",
                          "CAP": "00000",
                          "personalPhoneNumber": "Your Salesman Number..",
                          "shippingCosts": "0",
                          "instagramLink": "Your instagram link..",
                          "facebookLink": "Your facebook link..",
                          "whatsappLink": "Your whatsapp link..",
                          "questionsEmail": "Your questions email..",
                          "pushToken": null,
                          "chattingWith": null,
                          "extra1": "Not in use",
                        }, merge: true));
                  },
                  child: Container(
                    padding: EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[400],
                          spreadRadius: 0,
                          blurRadius: 3,
                        ),
                      ],
                      color: Colors.white,
                    ),
                    width: 20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.language, color: Color(0xFF7E7E7E)),
                            Text("Set Data To All Users",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black.withOpacity(0.5))),
                            Icon(Icons.arrow_forward_ios,
                                color: Color(0xFF7E7E7E)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),

     */
                //------------------- BUTTON TO SET DATA TO ALL THE LISTINGS OF THE APP ---------------
                // -------------------------------------------------------------------------------------------------------------------------------
/*
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),
                GestureDetector(
                  onTap: () async {
                    QuerySnapshot qr = await Firestore.instance
                        .collection('listings')
                        .getDocuments();
                    qr.documents.forEach((doc) => Firestore.instance
                            .collection('listings')
                            .document(doc.documentID)
                            .setData({
                          "type": "Seleziona",
                        }, merge: true));
                  },
                  child: Container(
                    padding: EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[400],
                          spreadRadius: 0,
                          blurRadius: 3,
                        ),
                      ],
                      color: Colors.white,
                    ),
                    width: 20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.language, color: Color(0xFF7E7E7E)),
                            Text("Set Data To All Listings",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black.withOpacity(0.5))),
                            Icon(Icons.arrow_forward_ios,
                                color: Color(0xFF7E7E7E)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),
*/

    //-------------------------------------------------------------------------------------------------------------------------------
                // ------------------ REPORTED LISTINGS -----------------
                Divider(),
                //new FlatButton(onPressed: _createAdminListing, child: Text("Admin Listing")),
                new Text(
                  "Reported Listings",
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.black.withOpacity(0.5)),
                  textAlign: TextAlign.center,
                ),
                Divider(),
                new Column(
                  children:
                      dataListings["reportedDocs"].toList().map<Widget>((v) {
                    return new ListTile(
                      title: Text(
                        v.data["title"].toString(),
                      ),
                      leading: Image.network(v.data["images"].first),
                      subtitle: Text(
                        v.data["description"],
                      ),
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(
                                    "What do you want to do with this listing?"),
                                actions: <Widget>[
                                  new FlatButton(
                                    child: Text("View"),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  new ViewListing(item: v)));
                                    },
                                  ),
                                  new FlatButton(
                                    child: Text("Delete"),
                                    onPressed: () async {
                                      await Firestore.instance
                                          .collection('listings')
                                          .document(v.documentID)
                                          .delete();
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  new AdminPage()));
                                    },
                                  ),
                                  new FlatButton(
                                    child: Text("Unreport"),
                                    onPressed: () async {
                                      await Firestore.instance
                                          .collection('listings')
                                          .document(v.documentID)
                                          .setData({
                                        "reported": false,
                                      }, merge: true);
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  new AdminPage()));
                                    },
                                  ),
                                ],
                              );
                            });
                      },
                    );
                  }).toList(),
                ),
              ],
            )
          : Center(child: CircularProgressIndicator()),
    );
  }
}
