import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'categories.dart';
import 'thanks.dart';
import '../main.i18n.dart';

//import 'package:carousel_slider/carousel_slider.dart';

class CreateListing extends StatefulWidget {
  CreateListing({Key key, this.category, this.type, this.item}) : super(key: key);

  final String category;
  final String type;
  dynamic item;

  @override
  _CreateListingState createState() => _CreateListingState();
}

class _CreateListingState extends State<CreateListing> {
  final descriptionController = TextEditingController();
  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  bool hidePhone = false;
  final priceController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final locationController = TextEditingController();
  String category;
  String type;

  List<dynamic> images = [];

  @override
  void initState() {
    category = widget.category;
    type = widget.type;
    if (widget.item != null) {
      nameController.text = widget.item["title"];
      priceController.text = widget.item["price"].toString();
      images = widget.item["images"];
      locationController.text = widget.item["location"];
      category = widget.item["category"];
      descriptionController.text = widget.item["description"];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        title:  GestureDetector(
          child: Text(
              category+" ▾",
              style: TextStyle(color: Colors.white, fontSize: 16)
          ),
          onTap: _changeCategory,
        ),
        actions: <Widget>[

          FlatButton(
            child: Text(widget.item == null ? "SELL".i18n : "UPDATE".i18n, style: TextStyle(color: Colors.white, fontSize: 16)),
            onPressed: _sell,
          ),
        ],
      ),

      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(40.0),
            topRight: const Radius.circular(40.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[300],
              spreadRadius: 4,
              blurRadius: 8,
            ),
          ],
        ),
        //----------------------------------------------------------------------------------
        padding: EdgeInsets.all(20),
        child: new FlatButton(
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: Color(0xFF58A9B1),
            ),
            width: MediaQuery.of(context).size.width * 0.8,
            child: Text(
              widget.item == null ? "Sell".i18n : "Update".i18n,
              style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
          ),
          onPressed: _sell,
        ),
        //----------------------------------------------------------------------------------
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: ListView(
          children: <Widget>[
            Divider(),
            new Container(
              height: 128,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: images.map<Widget>((v) {
                      return FlatButton(
                        onPressed: () {
                          setState(() {
                            images.remove(v);
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.all(0),
                          width: 128,
                          height: 128,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              image: NetworkImage(v),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    }).toList() +
                    [
                      FlatButton(
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.grey,
                          ),
                          child: Icon(
                            Icons.photo_camera,
                            color: Colors.white,
                            size: 32,
                          ),
                        ),
                        onPressed: () async {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              // return object of type Dialog
                              return WillPopScope(
                                onWillPop:  () async { return false; } ,
                                  child: AlertDialog(
                                title: new Text("Uploading image...".i18n),
                                content: new Container(width: 32, height: 32, child: Center(child: CircularProgressIndicator())),
                                      actions: <Widget>[
                                        new FlatButton(
                                          child: new Text("Cancel".i18n),
                                          onPressed: () { Navigator.pop(context); },
                                        ),
                                      ]
                              ));
                            },
                          );
                          File _newImage = await ImagePicker.pickImage(source: ImageSource.gallery);

                          final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child('product' + "-" + DateTime.now().millisecondsSinceEpoch.toString() + ".jpg");

                          firebaseStorageRef.putFile(_newImage).onComplete.then((v) async {
                            String url = await firebaseStorageRef.getDownloadURL();
                            setState(() {
                              images.add(url);
                            });
                            Navigator.pop(context);
                          });
                        },
                      ),
                    ],
              ),
            ),
            Divider(),

            if(category == "Clothing & Accessories" || category == "Vestiti & Accessori")...[
            Padding(padding: EdgeInsets.only(top: 3)),
            Center(child: Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, right: MediaQuery.of(context).size.width * 0.04, top: 10, bottom: 10),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black.withOpacity(0.5)),
                borderRadius: new BorderRadius.circular(25.0),
              ),
              child: GestureDetector(
              child: Text(
                  "Sottocategoria: " + type +" ▾",
    style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5)),
              ),
              onTap: _changeType,
            ),),),],

            Padding(padding: EdgeInsets.only(top: 3)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: "Item Name".i18n,
                  hintText: "Enter the name of the product...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 3)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: locationController,
                decoration: InputDecoration(
                  labelText: "Your Location".i18n,
                  hintText: "Enter your Location.. (ex: city, state)".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: priceController,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                decoration: InputDecoration(
                  labelText: "Price".i18n,
                  hintText: "Enter the Price...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: descriptionController,
                decoration: InputDecoration(
                  labelText: "Description".i18n,
                  hintText: "Enter the Description...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
                maxLines: 3,
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 10)),
            Divider(),
          ],
        ),
      ),
    );
  }

  void _sell() async {

    if (images.length > 0) {
      if ( nameController.text != "") {
        if (widget.item == null) {
            showDialog(context: context, builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(title: new Text("Adding listing...".i18n),
                content: new Container(width: 32,
                    height: 32,
                    child: Center(child: CircularProgressIndicator())),);
            },);

            FirebaseUser user = await FirebaseAuth.instance.currentUser();
            QuerySnapshot qs = await Firestore.instance.collection('users').where(
                'uid', isEqualTo: user.uid).getDocuments();

            await Firestore.instance.collection('listings').add({
            "title": nameController.text,
            "description": descriptionController.text,
            "seller": {
            "name": qs.documents.first.data["name"],
            "avatarURL": qs.documents.first.data["avatarURL"],
            "email": qs.documents.first.data["email"],
            "phoneNumber": qs.documents.first.data["hidePhone"] == true
            ? "Hidden"
                : qs.documents.first.data["phone"],
            "uid": user.uid,
            "userSiteUrl": qs.documents.first.data["userSiteUrl"] != null ? qs.documents.first.data["userSiteUrl"] : null,
            "call": qs.documents.first.data["call"],
            "pro": qs.documents.first.data["prof"] != null ? qs.documents.first.data["prof"] : null,
            "companyDescription": qs.documents.first.data["companyDescription"] != null ? qs.documents.first.data["companyDescription"] : null,
              "address": qs.documents.first.data["address"] != null ? qs.documents.first.data["address"] : null,
              "companySlogan": qs.documents.first.data["companySlogan"] != null ? qs.documents.first.data["companySlogan"] : null,
              "numberOfEmployees": qs.documents.first.data["numberOfEmployees"] != null ? qs.documents.first.data["numberOfEmployees"] : null,
              "yearsOfExperience": qs.documents.first.data["yearsOfExperience"] != null ? qs.documents.first.data["yearsOfExperience"] : null,
              "CAP": qs.documents.first.data["CAP"] != null ? qs.documents.first.data["CAP"] : null,
              "personalPhoneNumber": qs.documents.first.data["personalPhoneNumber"] != null ? qs.documents.first.data["personalPhoneNumber"] : null,
              "shippingCosts": qs.documents.first.data["shippingCosts"] != null ? qs.documents.first.data["shippingCosts"] : null,
              "instagramLink": qs.documents.first.data["instagramLink"] != null ? qs.documents.first.data["instagramLink"] : null,
              "facebookLink": qs.documents.first.data["facebookLink"] != null ? qs.documents.first.data["facebookLink"] : null,
              "whatsappLink": qs.documents.first.data["whatsappLink"] != null ? qs.documents.first.data["whatsappLink"] : null,
              "questionsEmail": qs.documents.first.data["questionsEmail"] != null ? qs.documents.first.data["questionsEmail"] : null,
            },
            "created": DateTime
                .now()
                .millisecondsSinceEpoch,
            "images": images,
            "price": priceController.text,
            "category": category,
            "type": type,
            "sold": "no",
            "location": locationController.text,
            });


            Navigator.pop(context);
            Navigator.pop(context);
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => new ThankYouPage()));

        } else {
          showDialog(context: context, builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(title: new Text("Updating listing...".i18n),
              content: new Container(width: 32,
                  height: 32,
                  child: Center(child: CircularProgressIndicator())),);
          },);

          //------------------------------------------------------------------



          //------------------------------------------------------------------

          FirebaseUser user = await FirebaseAuth.instance.currentUser();
          QuerySnapshot qs = await Firestore.instance.collection('users').where('uid', isEqualTo: user.uid).getDocuments();

          await Firestore.instance.collection('listings').document(widget.item.documentID).setData({

            "title": nameController.text,
            "description": descriptionController.text,
            "seller": {
              "name": qs.documents.first.data["name"],
              "avatarURL": qs.documents.first.data["avatarURL"],
              "email": qs.documents.first.data["email"],
              "phoneNumber": qs.documents.first.data["hidePhone"] == true
                  ? "Hidden"
                  : qs.documents.first.data["phone"],
              "uid": user.uid,
              "userSiteUrl": qs.documents.first.data["userSiteUrl"] != null ? qs.documents.first.data["userSiteUrl"] : null,
              "pro": qs.documents.first.data["prof"] != null ? qs.documents.first.data["prof"] : null,
              "companyDescription": qs.documents.first.data["companyDescription"] != null ? qs.documents.first.data["companyDescription"] : null,
              "address": qs.documents.first.data["address"] != null ? qs.documents.first.data["address"] : null,
              "companySlogan": qs.documents.first.data["companySlogan"] != null ? qs.documents.first.data["companySlogan"] : null,
              "numberOfEmployees": qs.documents.first.data["numberOfEmployees"] != null ? qs.documents.first.data["numberOfEmployees"] : null,
              "yearsOfExperience": qs.documents.first.data["yearsOfExperience"] != null ? qs.documents.first.data["yearsOfExperience"] : null,
              "CAP": qs.documents.first.data["CAP"] != null ? qs.documents.first.data["CAP"] : null,
              "personalPhoneNumber": qs.documents.first.data["personalPhoneNumber"] != null ? qs.documents.first.data["personalPhoneNumber"] : null,
              "shippingCosts": qs.documents.first.data["shippingCosts"] != null ? qs.documents.first.data["shippingCosts"] : null,
              "instagramLink": qs.documents.first.data["instagramLink"] != null ? qs.documents.first.data["instagramLink"] : null,
              "facebookLink": qs.documents.first.data["facebookLink"] != null ? qs.documents.first.data["facebookLink"] : null,
              "whatsappLink": qs.documents.first.data["whatsappLink"] != null ? qs.documents.first.data["whatsappLink"] : null,
              "questionsEmail": qs.documents.first.data["questionsEmail"] != null ? qs.documents.first.data["questionsEmail"] : null,
            },
            "created": DateTime
                .now()
                .millisecondsSinceEpoch,
            "images": images,
            "price": priceController.text,
            "category": category,
            "type": type,
            "sold": "no",
            "location": locationController.text,
          }, merge: true);

          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pop(context);
        }
      }
      else{
        Fluttertoast.showToast(msg: "Please add a title".i18n, timeInSecForIos: 3);
      }
    } else {
      Fluttertoast.showToast(msg: "Please add at least 1 image".i18n, timeInSecForIos: 3);
    }
  }

  void _changeCategory() async{
    Navigator.push(context, MaterialPageRoute(builder: (context) => new AllCategoriesPage())).then((v) {
      if (v != null) {
        setState(() {
          category = v;
        });
      }
    });
  }

  void _changeType() async{
    Navigator.push(context, MaterialPageRoute(builder: (context) => new AllClothesTypesPage())).then((v) {
      if (v != null) {
        setState(() {
          type = v;
        });
      }
    });
  }

  Future<List<String>> _uploadImages() async {
    List<String> _iurls = [];
  }
}

//--------------------- CREATE ADMIN ITEM --------------------------
//--------------------- CREATE ADMIN ITEM --------------------------
//--------------------- CREATE ADMIN ITEM --------------------------
//--------------------- CREATE ADMIN ITEM --------------------------
//--------------------- CREATE ADMIN ITEM --------------------------
//--------------------- CREATE ADMIN ITEM --------------------------
//--------------------- CREATE ADMIN ITEM --------------------------

class CreateAdminListing extends StatefulWidget {
  CreateAdminListing({Key key, this.item}) : super(key: key);

  final String category = "Warmie Affiliates".i18n;
  dynamic item;

  @override
  _CreateAdminListingState createState() => _CreateAdminListingState();
}

class _CreateAdminListingState extends State<CreateAdminListing> {
  final descriptionController = TextEditingController();
  final nameController = TextEditingController();
  final affiliateUrlController = TextEditingController();
  final phoneController = TextEditingController();
  bool hidePhone = false;
  final priceController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final locationController = TextEditingController();
  String category;

  List<dynamic> images = [];

  @override
  void initState() {
    category = widget.category;
    if (widget.item != null) {
      nameController.text = widget.item["title"];
      affiliateUrlController.text = widget.item["affiliateUrl"];
      priceController.text = widget.item["price"].toString();
      images = widget.item["images"];
      locationController.text = widget.item["location"];
      category = widget.item["category"];
      descriptionController.text = widget.item["description"];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        actions: <Widget>[
          FlatButton(
            child: Text(widget.item == null ? "SELL".i18n : "UPDATE".i18n, style: TextStyle(color: Colors.white, fontSize: 16)),
            onPressed: _sell,
          ),
        ],
      ),

      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(40.0),
            topRight: const Radius.circular(40.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[300],
              spreadRadius: 4,
              blurRadius: 8,
            ),
          ],
        ),
        //----------------------------------------------------------------------------------
        padding: EdgeInsets.all(20),
        child: new FlatButton(
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: Color(0xFF58A9B1),
            ),
            width: MediaQuery.of(context).size.width * 0.8,
            child: Text(
              widget.item == null ? "Sell".i18n : "Update".i18n,
              style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
          ),
          onPressed: _sell,
        ),
        //----------------------------------------------------------------------------------
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: ListView(
          children: <Widget>[
            Divider(),
            new Container(
              height: 128,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: images.map<Widget>((v) {
                  return FlatButton(
                    onPressed: () {
                      setState(() {
                        images.remove(v);
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: 128,
                      height: 128,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        image: DecorationImage(
                          image: NetworkImage(v),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  );
                }).toList() +
                    [
                      FlatButton(
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.grey,
                          ),
                          child: Icon(
                            Icons.photo_camera,
                            color: Colors.white,
                            size: 32,
                          ),
                        ),
                        onPressed: () async {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              // return object of type Dialog
                              return WillPopScope(
                                  onWillPop:  () async { return false; } ,
                                  child: AlertDialog(
                                    title: new Text("Uploading image...".i18n),
                                    content: new Container(width: 32, height: 32, child: Center(child: CircularProgressIndicator())),
                                  ));
                            },
                          );
                          File _newImage = await ImagePicker.pickImage(source: ImageSource.gallery);

                          final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child('product' + "-" + DateTime.now().millisecondsSinceEpoch.toString() + ".jpg");

                          firebaseStorageRef.putFile(_newImage).onComplete.then((v) async {
                            String url = await firebaseStorageRef.getDownloadURL();
                            setState(() {
                              images.add(url);
                            });
                            Navigator.pop(context);
                          });
                        },
                      ),
                    ],
              ),
            ),
            Divider(),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: "Item Name".i18n,
                  hintText: "Enter the name of the product...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 3)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: affiliateUrlController,
                decoration: InputDecoration(
                  labelText: "Item Url".i18n,
                  hintText: "Enter the url of the Amazon product...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            /*Padding(padding: EdgeInsets.only(top: 3)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: locationController,
                decoration: InputDecoration(
                  labelText: "Your Location".i18n,
                  hintText: "Enter your Location...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),*/
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: priceController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Price".i18n,
                  hintText: "Enter the Price...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: descriptionController,
                decoration: InputDecoration(
                  labelText: "Description".i18n,
                  hintText: "Enter the Description...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
                maxLines: 3,
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 10)),
            Divider(),
          ],
        ),
      ),
    );
  }

  void _sell() async {
    if (images.length > 0) {
      if ( nameController.text != "") {
        if (widget.item == null) {
          showDialog(context: context, builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(title: new Text("Adding listing...".i18n),
              content: new Container(width: 32,
                  height: 32,
                  child: Center(child: CircularProgressIndicator())),);
          },);

          FirebaseUser user = await FirebaseAuth.instance.currentUser();
          QuerySnapshot qs = await Firestore.instance.collection('users').where(
              'uid', isEqualTo: user.uid).getDocuments();


          await Firestore.instance.collection('listings').add({
            "title": nameController.text,
            "description": descriptionController.text,
            "seller": {
              "name": qs.documents.first.data["name"],
              "avatarURL": qs.documents.first.data["avatarURL"],
              "email": qs.documents.first.data["email"],
              "phoneNumber": qs.documents.first.data["hidePhone"] == true
                  ? "Hidden"
                  : qs.documents.first.data["phone"],
              "uid": user.uid,
              "userSiteUrl": qs.documents.first.data["userSiteUrl"],
              "pro": qs.documents.first.data["prof"],
              "call": qs.documents.first.data["call"]
            },
            "created": DateTime
                .now()
                .millisecondsSinceEpoch,
            "images": images,
            "price": priceController.text,
            "category": category,
            "location": "Warmie",
            "affiliateUrl": affiliateUrlController.text,
          });


          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => new ThankYouPage()));
        } else {
          showDialog(context: context, builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(title: new Text("Updating listing...".i18n),
              content: new Container(width: 32,
                  height: 32,
                  child: Center(child: CircularProgressIndicator())),);
          },);

          FirebaseUser user = await FirebaseAuth.instance.currentUser();
          QuerySnapshot qs = await Firestore.instance.collection('users').where(
              'uid', isEqualTo: user.uid).getDocuments();


          await Firestore.instance.collection('listings').document(
              widget.item["documentID"]).setData({
            "title": nameController.text,
            "description": descriptionController.text,
            "seller": {
              "name": qs.documents.first.data["name"],
              "avatarURL": qs.documents.first.data["avatarURL"],
              "email": qs.documents.first.data["email"],
              "phoneNumber": qs.documents.first.data["hidePhone"] == true
                  ? "Hidden"
                  : qs.documents.first.data["phone"],
              "uid": user.uid,
              "userSiteUrl": qs.documents.first.data["userSiteUrl"],
              "pro": qs.documents.first.data["prof"],
              "call": qs.documents.first.data["call"]
            },
            "created": DateTime
                .now()
                .millisecondsSinceEpoch,
            "images": images,
            "price": priceController.text,
            "category": category,
            "location": "Warmie",
            "affiliateUrl": affiliateUrlController.text,
          }, merge: true);

          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pop(context);
        }
      }
      else{
        Fluttertoast.showToast(msg: "Please add a title".i18n, timeInSecForIos: 3);
      }
    } else {
      Fluttertoast.showToast(msg: "Please add at least 1 image".i18n, timeInSecForIos: 3);
    }
  }

  Future<List<String>> _uploadImages() async {
    List<String> _iurls = [];
  }
}