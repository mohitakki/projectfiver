import 'package:flutter/material.dart';
import 'package:warmie/feed.dart';
import '../main.i18n.dart';

class ThankYouPage extends StatefulWidget {
  @override
  _ThankYouPageState createState() => _ThankYouPageState();
}

class _ThankYouPageState extends State<ThankYouPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.push(context, MaterialPageRoute(builder: (context) => new FeedPage()));
        return false;
      },
      child: Material(
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 4,
                alignment: Alignment.center,
                child: Text(
                "Congratulations your product \nHas been published!".i18n,
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w800,
                ),
                textAlign: TextAlign.center,
              ),),
              new Column(
                children: <Widget>[
                  new Image.asset('assets/ecology.png'),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3,
                    color: Color(0xFF62AEB6),
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text("Thank you for supporting Warmie \n We are stopping the global warming with your help".i18n, style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18,), textAlign: TextAlign.center,),
                        Padding(padding: EdgeInsets.all(20)),
                        new RawMaterialButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => new FeedPage()));
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: 200,
                            height: 48,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              border: Border.all(color: Colors.white,),
                            ),
                            child: Text("Continue".i18n , style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18,),),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
