import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../main.i18n.dart';
/*
UNUSED PACKAGES

import 'package:fluttertoast/fluttertoast.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'create.dart';
*/

//--------------------------------------------------------- ALL CATEGORIES PAGE -----------------------------------------------------------

class AllCategoriesPage extends StatefulWidget {
  AllCategoriesPage({Key key, this.category}) : super(key: key);

  final String category;

  @override
  _AllCategoriesPageState createState() => _AllCategoriesPageState();
}

class _AllCategoriesPageState extends State<AllCategoriesPage> {
  final descriptionController = TextEditingController();
  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  bool hidePhone = false;
  final priceController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  List<List<String>> categories = [
    ["Clothing & Accessories".i18n, "tshirt.png"],
    ["Home & Garden".i18n, "garden.png"],
    ["Childhood, Children, Baby".i18n, "children.png"],
    ["Sports, Freetime".i18n, "sports.png"],
    ["Games & Consoles".i18n, "consoles.png"],
    ["Animals and Accessories".i18n, "pets.png"],
    ["Movie, Books and Music".i18n, "books.png"],
    ["Electronics".i18n, "smartphone.png"],
    ["Cars & Accessories".i18n, "cars.png"],
    ["Motorbike & Accessories".i18n, "motorbike.png"],
    ["Bikes & Accessories".i18n, "bikes.png"],
    ["Property On Sale".i18n, "house.png"],
    ["Property For Rent".i18n, "house.png"],
    ["Collectibles & Art".i18n, "art.png"],
    ["Jobs".i18n, "jobs.png"],
    ["Services".i18n, "services.png"],
    ["Others".i18n, "others.png"],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        title: Text("Categories".i18n),

      ),
      body: ListView(
        children: categories.map<Widget>((v) {
          return ListTile(
            leading: Image.asset("assets/" + v[1], width: 32, height: 32,),
            title: Text(v[0]),
            trailing: Icon(Icons.arrow_forward_ios, color: Colors.grey,),
            onTap: () {
              Navigator.pop(context, v[0]);

            },
          );
        }).toList(),
      ),
    );
  }
}

//-------------------------------------------------- ALL LOCATION PAGE ----------------------------------------------------

class AllLocationPage extends StatefulWidget {
  AllLocationPage({Key key, this.where}) : super(key: key);

  final String where;

  @override
  _AllLocationPageState createState() => _AllLocationPageState();
}

class _AllLocationPageState extends State<AllLocationPage> {

  List<List<String>> where = [
    ["italy".i18n, ""],
    ["usa".i18n, ""],
    ["france".i18n, ""],
    ["england".i18n, ""],
    ["spain".i18n, ""],
    ["china".i18n, ""],
    ["brazil".i18n, ""],
    ["japan".i18n, ""],
    ["germany".i18n, ""],
    ["mexico".i18n, ""],
    ["canada".i18n, ""],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        title: Text("Location".i18n),

      ),
      body: ListView(
        children: where.map<Widget>((v) {
          return ListTile(
            leading: Image.asset("assets/" + v[1], width: 32, height: 32,),
            title: Text(v[0]),
            trailing: Icon(Icons.arrow_forward_ios, color: Colors.grey,),
            onTap: () {
              Navigator.pop(context, v[0]);

            },
          );
        }).toList(),
      ),
    );
  }
}

//-------------------------------------------------- ALL CLOTHES TYPES PAGE ----------------------------------------------------

class AllClothesTypesPage extends StatefulWidget {
  AllClothesTypesPage({Key key, this.type}) : super(key: key);

  final String type;

  @override
  _AllClothesTypesState createState() => _AllClothesTypesState();
}

class _AllClothesTypesState extends State<AllClothesTypesPage> {

  List<List<String>> type = [
    ["Accessories".i18n, "accessories.png"],//acc
    ["Handbags".i18n, "handbags.png"],//bo
    ["Hats".i18n, "hats.png"],//cap
    ["Swimwear".i18n, "swimwear.png"],//cos
    ["Sweatshirts".i18n, "sweatshirts.png"],//felp
    ["Jackets".i18n, "jackets.png"],//giac
    ["Jewelry".i18n, "jewelry.png"],//giac
    ["Skirts".i18n, "skirt.png"],//gon
    ["Underwear".i18n, "underwear.png"],//int
    ["Glasses".i18n, "sunglasses.png"],//oc
    ["Watches".i18n, "watches.png"],//or
    ["Trousers".i18n, "trousers.png"],//pant
    ["Wallets".i18n, "wallets.png"],//po
    ["Shoes".i18n, "shoes.png"],//scar
    ["Shorts".i18n, "shorts.png"],//sho
    ["Tops".i18n, "top.png"],//top
    ["T-Shirt".i18n, "tshirt.png"],//t-Shirt
    ["Dresses".i18n, "dress.png"],//vest

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        title: Text("Clothes Types".i18n),

      ),
      body: ListView(
        children: type.map<Widget>((v) {
          return ListTile(
            leading: Image.asset("assets/" + v[1], width: 32, height: 32,),
            title: Text(v[0]),
            trailing: Icon(Icons.arrow_forward_ios, color: Colors.grey,),
            onTap: () {
              Navigator.pop(context, v[0]);
            },
          );
        }).toList(),
      ),
    );
  }
}