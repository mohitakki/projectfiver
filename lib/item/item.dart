import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import '../conversation.dart';
import '../profile.dart';
import 'create.dart';
import '../main.i18n.dart';
import 'package:warmie/admob_service.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';

/*

UNUSED PACKAGES

import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'categories.dart';

*/


class ViewListing extends StatefulWidget {
  ViewListing({Key key, this.item}) : super(key: key);

  final DocumentSnapshot item;
  @override
  _ViewListingState createState() => _ViewListingState();
}

class _ViewListingState extends State<ViewListing> {
  FirebaseUser user;
  int _current = 0;
  final ams = AdMobService();
  @override
  void initState() {
    super.initState();
    Admob.initialize(ams.getAdMobAppId());
    _checkLogin();
  }

  void _checkLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    String c = widget.item["category"];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text(""),
        actions: <Widget>[
          widget.item["seller"]["uid"] != user.uid ? new FlatButton() : new FlatButton(
            child: Text("EDIT".i18n, style: TextStyle(color: Colors.white, fontSize: 16),),
            onPressed: () {

              Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(item: widget.item)));
            },
          ),
          IconButton(
            icon: Icon(Icons.more_horiz,color: Colors.white),
            onPressed: () {
              showCupertinoModalPopup(
                  context: context,
                  builder: (BuildContext context) {
                    return CupertinoActionSheet(
                      actions: <Widget>[
                        CupertinoActionSheetAction(
                          child: Text("Report item".i18n, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.red,),),
                          onPressed: () async {
                            Firestore.instance.collection('listings').document(widget.item.documentID).setData({
                              "reported": true,
                            }, merge: true);
                            Fluttertoast.showToast(msg: "Report submitted.");
                          },
                        ),
                      ],
                      cancelButton: CupertinoActionSheetAction(
                        child: Text("Cancel".i18n, style: TextStyle(fontWeight: FontWeight.w900,),),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    );
                  }
              );
            },
          ),
        ],
      ),

      bottomNavigationBar: Container(
        height: 80,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(40.0),
            topRight: const Radius.circular(40.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[300],
              spreadRadius: 4,
              blurRadius: 8,
            ),
          ],
        ),
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(widget.item["price"].toString() + " €", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20, color: Color(0xFF58A9B1))),
            user == null || widget.item["seller"]["uid"] != user.uid ? new FlatButton(

              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: Color(0xFF58A9B1),
                ),
                width: MediaQuery.of(context).size.width * 0.3,
                child: Text(
                  "Chat",
                  style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () async {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title: new Text("Creating conversation..."),
                      content: new Container(width: 32, height: 32, child: Center(child: CircularProgressIndicator())),
                    );
                  },
                );
                FirebaseUser user = await FirebaseAuth.instance.currentUser();
                QuerySnapshot qs = await Firestore.instance.collection('users').where('uid', isEqualTo: user.uid).getDocuments();

                DocumentReference ds = await Firestore.instance.collection('inbox').add({
                  "item": {
                    "title": widget.item["title"],
                    "price": widget.item["price"],
                    "image": widget.item["images"][0],
                    "docRef": widget.item.reference.documentID,
                  },
                  "users": [
                    user.uid,
                    widget.item["seller"]["uid"],
                  ],
                  "messages": [{
                    "created": DateTime.now().millisecondsSinceEpoch,
                    "body": "Ciao, Sono interessato a questo oggetto! :D".i18n,
                    "sender": 0,
                  },],
                  "userData": [
                {
                  "name": qs.documents.first.data["name"],
                  "avatarURL": qs.documents.first.data["avatarURL"],
                },
                {
                  "name": widget.item["seller"]["name"],
                  "avatarURL": widget.item["seller"]["avatarURL"],
                },
                  ],
                });

                DocumentSnapshot doc = await ds.snapshots().first;
                dynamic v = doc.data;

                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => new Conversation(title: v["users"][0] == user.uid ? v["userData"][0]["name"] : v["userData"][1]["name"], avatarURL:  v["users"][0] == user.uid ? v["userData"][0]["avatarURL"] : v["userData"][1]["avatarURL"], documentID: ds.documentID, item: {"title": v["item"]["title"], "image": v["item"]["image"], "price": v["item"]["price"]}, us: v["users"][0] == user.uid ? 0 : 1)));

              }, //----------------------------------------------------------------------------------
            ) : new FlatButton(
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: Colors.red,
                ),
                width: MediaQuery.of(context).size.width * 0.3,
                child: Text(
                  "Delete".i18n,
                  style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () async {
                Navigator.pop(context);

                Firestore.instance.collection('listings').document(widget.item.documentID).delete();
              },
            ),
            //----------------------------------------------------------------------------------
            Container(),
          ],
        ),
      ),
      //---------------------------------------------------- ITEM ON SALE PAGE --------------------------------------------------
      body: Padding(
        padding: EdgeInsets.only(left: 0),
        child: ListView(
          children: <Widget>[
            // --- IMAGES OF THE ITEM ---
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.transparent,
                    blurRadius: 10,
                    offset: Offset(0, 6),
                  ),
                ],
              ),
              child: Column(
                children: [
                  CarouselSlider(
                  options: CarouselOptions(
                    height: 300.0,
                    aspectRatio: 1,
                    viewportFraction: 1.0,
                    enableInfiniteScroll: false,
                  ),
                  items: widget.item["images"].map<Widget>((v) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                              image: CachedNetworkImageProvider(
                                v,
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }).toList(),
                ),
                ]
              )
            ),

            //---- TITLE OF THE ITEM ----
            new Container(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
                top: 20,
                bottom: 40,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30),topRight: Radius.circular(30),),
              ),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          widget.item["title"],
                          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 24),
                          maxLines: 3,
                      ),
                      Padding(padding: EdgeInsets.only(top: 3)),
                    ],
                  ),

                  //--------------------------------------------------- USER DETAILS BOX --------------------------------------------
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => new ProfilePage(thisUser: widget.item["seller"],showBack: true)));
                    },
                    child: new Container(
                    height: 190,
                    margin: EdgeInsets.all(10),
                    child: new Stack(
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(top: 20),
                          padding: EdgeInsets.only(left: 20, right: 20, top: 50, bottom: 0),
                          height: 170,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[300],
                                blurRadius: 10,
                                offset: Offset(0, 0),
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text(widget.item["seller"]["name"], style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 16,)),
                                  ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text(widget.item["location"], style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),maxLines: 7,),
                                ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text(DateFormat('d/MM/yy hh:s').format(DateTime.fromMillisecondsSinceEpoch(widget.item["created"])), style: TextStyle(fontSize: 14, color: Colors.grey)),
                                ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  if(c.i18n != "Vestiti & Accessori" )...[
                                  new Text(c.i18n, style: TextStyle(fontSize: 14, color: Colors.grey)),]
                                  else if(c.i18n == "Vestiti & Accessori" && widget.item["type"] != "Seleziona")...[
                                    new Text(c.i18n + " | " + widget.item["type"], style: TextStyle(fontSize: 14, color: Colors.grey)),]
                                  else...[
                                      new Text(c.i18n, style: TextStyle(fontSize: 14, color: Colors.grey)),
                                    ]
                                  // ---------- THE BELOW PART IS COMMENTED BECAUSE THE GO TO PROFILE FUNCTION, DOESN'T WORK --------------
                                ],),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new Text("View Online Items".i18n, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14, color: Color(0xFF58A9B1))),
                                    // ---------- THE BELOW PART IS COMMENTED BECAUSE THE GO TO PROFILE FUNCTION, DOESN'T WORK --------------
                                  ],
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          left: MediaQuery.of(context).size.width / 2 - 64,
                          child: Container(
                            width: 64,
                            height: 64,
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[300],
                                  blurRadius: 10,
                                  offset: Offset(0, 0),
                                ),
                              ],
                              borderRadius: BorderRadius.circular(50),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(
                                  widget.item["seller"]["avatarURL"],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],),
                    ),
                  ),

                  if(widget.item["seller"]["uid"] == user.uid)...[
                    if(widget.item["sold"] == "no")...[
                    FlatButton(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.red,
                        ),
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Text(
                          "Mark As Sold".i18n,
                          style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      onPressed: () async {
                        Navigator.pop(context);

                        Firestore.instance.collection('listings').document(widget.item.documentID).setData({
                          "sold": "si"
                        },merge: true
                        );
                      },
                    ),
                    ]
                    else...[
                      FlatButton(
                        child: Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: Colors.green,
                          ),
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: Text(
                            "Marked As Sold.".i18n,
                            style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);

                          Firestore.instance.collection('listings').document(widget.item.documentID).setData({
                            "sold": "no"
                          }, merge: true
                          );
                        },
                      ),
                    ]
                  ],

                  if(widget.item["seller"]["pro"]!= "no")
                    if(widget.item["seller"]["userSiteUrl"].toString()!= "Your Website..")
                    Container(
                      padding: EdgeInsets.all(0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new GestureDetector(
                            onTap: () {
                              String website = widget.item["seller"]["userSiteUrl"].toString();
                              print(website.substring(0,4));
                              if(website.substring(0,4) == "http"){
                                print(website);
                                launch(website);
                              }
                              else{
                                print(website);
                                launch("http://"+website);
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 30, right: 30, top: 3, bottom: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Color(0xFF58A9B1),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[300],
                                    blurRadius: 10,
                                    offset: Offset(0, 0),
                                  ),
                                ],
                              ),
                              child: Text(
                                "Website".i18n,
                                style: TextStyle(color: Colors.white, fontSize: 18,fontWeight: FontWeight.w600),
                              ),
                            ),),
                          if(widget.item["seller"]["call"].toString()!= "Your Phone Number.." || widget.item["seller"]["personalPhoneNumber"].toString()!= "Your Salesman Number..")...[
                            new GestureDetector(
                            onTap: () {
                              showCupertinoModalPopup(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return CupertinoActionSheet(
                                      actions: <Widget>[
                                        if(widget.item["seller"]["personalPhoneNumber"].toString()!= "Your Salesman Number..")...[
                                          CupertinoActionSheetAction(
                                            child: Text("Personale: ".i18n + widget.item["seller"]["personalPhoneNumber"].toString(), style: TextStyle(fontWeight: FontWeight.w600, ),),
                                            onPressed: () async {
                                            },
                                          ),],
                                        if(widget.item["seller"]["call"].toString()!= "Your Phone Number..")...[
                                          CupertinoActionSheetAction(
                                            child: Text("Ufficio: ".i18n + widget.item["seller"]["call"].toString(), style: TextStyle(fontWeight: FontWeight.w600, ),),
                                            onPressed: () async {
                                            },
                                          ),]
                                      ],
                                      cancelButton: CupertinoActionSheetAction(
                                        child: Text("Cancel".i18n, style: TextStyle(fontWeight: FontWeight.w700,color: Colors.red,),),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      ),
                                    );
                                  }
                              );
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 45, right: 45, top: 3, bottom: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Color(0xFF58A9B1),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[300],
                                    blurRadius: 10,
                                    offset: Offset(0, 0),
                                  ),
                                ],
                              ),
                              child: Text(
                                "Call".i18n,
                                style: TextStyle(color: Colors.white, fontSize: 18,fontWeight: FontWeight.w600),
                              ),
                            ),),]
                        ],
                      ),
                    ),
                  if(widget.item["seller"]["pro"]!=null)
                  Padding(padding: EdgeInsets.only(bottom: 30),),
                  AdmobBanner(
                    adUnitId: ams.getBannerAdId1(),
                    adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 30),),

                  //------------------------------------- DESCRIPTION BOX -------------------------------------------
                  new Container(
                      width: 350,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 40,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 20,
                            offset: Offset(0, 0),
                          ),
                        ],
                      ),
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            new Text("Details".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 18, )),
                            Padding(padding: EdgeInsets.only(top: 20)),
                            new Text(widget.item["description"], style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 16)),
                            Padding(padding: EdgeInsets.only(top: 3)),
                          ]
                      )
                  ),
                  Padding(padding: EdgeInsets.only(top: 30),),
                  AdmobBanner(
                    adUnitId: ams.getBannerAdId2(),
                    adSize: AdmobBannerSize.FULL_BANNER,
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 30),),
                  //------------------------------------- DESCRIPTION BOX -------------------------------------------
                  new Container(
                      width: 350,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 40,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 20,
                            offset: Offset(0, 0),
                          ),
                        ],
                      ),
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            new Text("Useful Information".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 18, )),
                            Padding(padding: EdgeInsets.only(top: 20)),
                            new Text("- Avoid scams by acting locally or by paying with PayPal Never pay with Western Union, Moneygram or other anonymous payment services \n \n - Don't buy or sell outside your country. Don't accept cashiers checks outside your country \n \n - This app is not involved in any transaction and does not handle payments, purchases, guarantee transactions, electronic payment services, or buyer protection or sales certificates offers.", style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 14)),
                            Padding(padding: EdgeInsets.only(top: 3)),
                          ]
                      )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 20),),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
