import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'main.i18n.dart';
import 'item/item.dart';
import 'admob_service.dart';


class FilteredSearchPage extends StatefulWidget {
  @override
  FilteredSearchPage({@required this.filter, this.type, this.showBack});

  String filter;
  String type;
  bool showBack;

  _FilteredSearchPageState createState() => _FilteredSearchPageState();
}

class _FilteredSearchPageState extends State<FilteredSearchPage> {
  final ams = AdMobService(); //this is used for the ads in the app
  final searchController = TextEditingController();
  Map<String, String> search;

  get tuser => null;
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Color(0xFF58A9B1),
      ),
      child: Column(
        children: <Widget>[
          if(widget.type == "select")...[
            AppBar(
              backgroundColor: Color(0xFF58A9B1),
              title: Text(widget.filter),
            ),
          ]else...[
            AppBar(
              backgroundColor: Color(0xFF58A9B1),
              title: Text(widget.type),
            ),
          ],

          if(widget.type == "select")...[
          //------------------------------------------------ LIST ITEM ON SALE --------------------------------------------------
          Expanded(
            child: Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
              child: StreamBuilder(
                  stream: Firestore.instance.collection('listings').where("category", isEqualTo: widget.filter).snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
                    if (snapshot.data.documents.length == 0) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text("There's 0 item on sale \n Try to sell something \n You don't use anymore".i18n,
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                                color: Colors.grey,
                              )),
                        ],
                      );
                    }
                    bool _display = true;
                    DocumentSnapshot v;
                    //----------------------------------------------- THIS ARE THE ITEMS ---------------------------------
                    return GridView.count(
                      crossAxisCount: 3,
                      children: snapshot.data.documents.where((DocumentSnapshot v){

                        //-----------------------------------------------------------------

                        print(v.data['sold']);
                        if(v.data['sold'] == "si")
                        {_display = false;}
                        else{
                          _display = true;
                        }

                        print(_display);
                        return _display;})
                          .map<Widget>((DocumentSnapshot v) {

                        //-----------------------------------------------------------------
                        if (v["category"].toString() == widget.filter)
                        {
                            _display = true;
                        }
                        else {
                          _display = false;
                        }

                        if (_display == true && search == null) {
                          return Container(
                              padding: EdgeInsets.all(0),
                              margin: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[400],
                                    spreadRadius: 0,
                                    blurRadius: 5,
                                  ),
                                ],
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  v.data["documentID"] = v.documentID;
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                      new ViewListing(item: v)));
                                },
                                child: GridTile(
                                  child: Container(
                                    width: 64,
                                    height: 64,
                                    decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey[400],
                                          spreadRadius: 0,
                                          blurRadius: 5,
                                        ),
                                      ],
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: CachedNetworkImageProvider(
                                          v["images"][0],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                          );
                        }
                        else{
                          return Container(
                              padding: EdgeInsets.all(0),
                              margin: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[400],
                                    spreadRadius: 0,
                                    blurRadius: 5,
                                  ),
                                ],
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  v.data["documentID"] = v.documentID;
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                      new ViewListing(item: v)));
                                },
                                child: Card(
                                  child: Container(
                                    width: 64,
                                    height: 64,
                                    decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey[400],
                                          spreadRadius: 0,
                                          blurRadius: 5,
                                        ),
                                      ],
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: CachedNetworkImageProvider(
                                          v["images"][0],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                          );
                        }
                        return Container();
                      }).toList(),
                    );
                  }),
            ),
          ),
          ] 
          else...[
            Expanded(
              child: Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                child: StreamBuilder(
                    stream: Firestore.instance.collection('listings').where("category", isEqualTo: widget.filter).where("type", isEqualTo: widget.type).snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
                      if (snapshot.data.documents.length == 0) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text("There's 0 item on sale \n Try to sell something \n You don't use anymore".i18n,
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,
                                  color: Colors.grey,
                                )),
                          ],
                        );
                      }
                      bool _display = true;
                      DocumentSnapshot v;
                      //----------------------------------------------- THIS ARE THE ITEMS ---------------------------------
                      return GridView.count(
                        crossAxisCount: 3,
                        children: snapshot.data.documents.where((DocumentSnapshot v){
                          if (searchController.text != "")
                          {
                            _display = true;
                            String titlePlusLocation = v["title"].toString() + " " + v["location"].toString();
                            // print(searchController.text);
                            String splittedSearch = searchController.text;
                            List<String> words = splittedSearch.split(" ");

                            // print(titlePlusLocation);
                            if(v["title"].toString().toLowerCase().contains(searchController.text.toLowerCase()) || v["title"].toString().toUpperCase().contains(searchController.text.toUpperCase()) || v["title"].contains(searchController.text))
                            {
                              _display = true;
                            }
                            else if(v["location"].toString().toLowerCase().contains(searchController.text.toLowerCase()) || v["location"].toString().toUpperCase().contains(searchController.text.toUpperCase()))
                            {
                              _display = true;
                            }
                            else if(titlePlusLocation.toLowerCase().contains(words[0].toLowerCase()) || titlePlusLocation.toString().toUpperCase().contains(words[0].toUpperCase()))
                            {
                              _display = true;
                            }
                            else
                            {
                              _display = false;
                            }
                          }
                          //-----------------------------------------------------------------
                          if (search != null) {
                            if (search["category"] != "All the categories" || search["category"] != "Tutte le categorie") {
                              (v["category"] == search["category"])||(v["category"] == search["category"].i18n) ? _display = true : _display = false;
                            }
                            if (search["location"] != "All the world" || search["location"] != "Tutto il mondo") {
                              (v["location"] == search["location"]) || (v["location"] == search["location".i18n])? _display = true : _display = false;
                            }
                          }

                          print(v.data['sold']);
                          if(v.data['sold'] == "si")
                          {_display = false;}
                          else{
                            _display = true;
                          }

                          print(_display);
                          return _display;}).map<Widget>((DocumentSnapshot v) {

                          //-----------------------------------------------------------------
                          if (v["category"].toString() == widget.filter)
                          {
                            _display = true;
                          }
                          else {
                            _display = false;
                          }

                          //------------------------------------------
                          if (_display == true && search == null) {
                            return Container(
                                padding: EdgeInsets.all(0),
                                margin: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey[400],
                                      spreadRadius: 0,
                                      blurRadius: 5,
                                    ),
                                  ],
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    v.data["documentID"] = v.documentID;
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) =>
                                        new ViewListing(item: v)));
                                  },
                                  child: GridTile(
                                    child: Container(
                                      width: 64,
                                      height: 64,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey[400],
                                            spreadRadius: 0,
                                            blurRadius: 5,
                                          ),
                                        ],
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: CachedNetworkImageProvider(
                                            v["images"][0],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                            );
                          }
                          else{
                            return Container(
                                padding: EdgeInsets.all(0),
                                margin: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey[400],
                                      spreadRadius: 0,
                                      blurRadius: 5,
                                    ),
                                  ],
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    v.data["documentID"] = v.documentID;
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) =>
                                        new ViewListing(item: v)));
                                  },
                                  child: Card(
                                    child: Container(
                                      width: 64,
                                      height: 64,
                                      decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey[400],
                                            spreadRadius: 0,
                                            blurRadius: 5,
                                          ),
                                        ],
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: CachedNetworkImageProvider(
                                            v["images"][0],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                            );
                          }
                          return Container();
                        }).toList(),
                      );
                    }),
              ),
            ),
          ]
        ],
      ),
    );
  }
}