import 'package:flutter/material.dart';
import 'login.dart';
import 'signupWithEmail.dart';
import 'package:warmie/main.i18n.dart';

//import 'package:carousel_slider/carousel_slider.dart';

class SignupPage extends StatefulWidget {
  SignupPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 5)),
            new Text("Sign up".i18n, style: TextStyle(color: Color(0xFF2C666E), fontSize: 28, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
            Padding(padding: EdgeInsets.only(top: 10)),
            new Text("Buy and sell beautiful \nthings and help the world :D".i18n, style: TextStyle(color: Colors.black.withOpacity(0.5), fontSize: 16, fontWeight: FontWeight.w700), textAlign: TextAlign.center,),
            Padding(padding: EdgeInsets.only(top: 15)),
            Padding(padding: EdgeInsets.only(top: 5)),
            new FlatButton(
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color(0xFFE3E8EB),
                ),
                width: MediaQuery.of(context).size.width*0.8,
                child: Text("Continue with Email".i18n, style: TextStyle(fontSize: 18.0, color: Color(0xFF2C666E), fontWeight: FontWeight.w500), textAlign: TextAlign.center,),),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new SignupWithEmailPage()));
              },
            ),
            Padding(padding: EdgeInsets.only(top: 20)),

            /*new FlatButton(
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color(0xFF4D67AD),
                ),
                width: MediaQuery.of(context).size.width*0.8,
                child: Text("F", style: TextStyle(fontSize: 16.0, color: Colors.white, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),),
              onPressed: () {

              },
            ),
            Padding(padding: EdgeInsets.only(top: 10)),
            new FlatButton(
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color(0xFF5383EC),
                ),
                width: MediaQuery.of(context).size.width*0.8,
                child: Text("Continue with Google", style: TextStyle(fontSize: 16.0, color: Colors.white, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),),
              onPressed: () {

              },
            ),*/

            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                new Text(
                  "Already a member?  ".i18n, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                ),
                FlatButton(
                  child: Container(
                    padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Color(0xFFE3E8EB),
                      ),
                    width: MediaQuery.of(context).size.width*0.18,
                    child: Text("Login", style: TextStyle(fontSize: 16.0, color: Color(0xFF2C666E), fontWeight: FontWeight.w500), textAlign: TextAlign.center,),),
                      onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => new LoginPage()));
                    },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
