import 'package:auto_localization/auto_localization.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'signup.dart';
import 'package:warmie/main.i18n.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';

class OnboardingPage extends StatefulWidget {
  OnboardingPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  List<List<String>> intro = [
      ["You earn, We Plant trees.".i18n, "With this app we want to help fight \n global warming and climate change.".i18n, "Attachment_1569346780.jpeg", "Get Started".i18n, "0"],
    ["Buy and Sell".i18n, "You buy and sell things \n and you take all the money.".i18n, "Attachment_1570627479.jpeg", "Next".i18n, "1"],
    ["How we earn money".i18n, "In your search there are ads, \n from these ads we generate revenue.".i18n, "Attachment_1570881820.jpeg", "Next".i18n, "2"],
    ["Circular Economy".i18n, "By reusing used things we can reduce the \n CO2 emissions caused by their \n production and reduce waste.".i18n, "illustration_4.jpg", "Next".i18n, "3"],
    ["We plant trees".i18n, "We plant trees and in doing so \n we reduce CO2 emissions and help \n reduce the advance of global warming.".i18n, "ecology.png", "Get Started".i18n, "4"],
  ];

  CarouselSlider carouselCtrl = CarouselSlider(items: <Widget>[],);
  CarouselController carouselController = CarouselController();

  @override
  void initState() {
    super.initState();
  }

  void _setupCarousel() {
    setState(() {
      carouselCtrl = CarouselSlider(
        carouselController: carouselController,
        options: CarouselOptions(
        height: MediaQuery.of(context).size.height,
        viewportFraction: 1.0,
        enableInfiniteScroll: false,),
        items: intro.map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget> [
                    Padding(padding: EdgeInsets.all(20)),
                    i[4] == "4" ? Container(alignment: Alignment.bottomCenter, height: MediaQuery.of(context).size.height/2, width: MediaQuery.of(context).size.width*0.8, child: Image.asset("assets/" + i[2])) : Image.asset("assets/" + i[2], height: MediaQuery.of(context).size.height/2, fit: BoxFit.cover,),
                    Expanded(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height/2,
                        decoration: BoxDecoration(
                          color: Color(000000000),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget> [
                            Text(i[0], style: TextStyle(fontSize: 20.0, color: Color(0xFF2C666E), fontWeight: FontWeight.w700), textAlign: TextAlign.center,),
                            Text(i[1], style: TextStyle(fontSize: 16.0, color: Colors.black.withOpacity(0.5), fontWeight: FontWeight.w700), textAlign: TextAlign.center,),
                            i[4] == "0" ? Text("") : Text(i[4] + " of 4".i18n, style: TextStyle(fontSize: 16.0, color: Color(0xFF2C666E), fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                            new FlatButton(
                              child: Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF58A9B1).withOpacity(0.5),
                                      blurRadius: 5.0, // has the effect of softening the shadow
                                      spreadRadius: 0, // has the effect of extending the shadow
                                      offset: Offset(
                                        0.0, // horizontal, move right 10
                                        0.0, // vertical, move down 10
                                      ),
                                    )
                                  ],
                                  borderRadius: BorderRadius.circular(30),
                                  color: Color(0xFF58A9B1),
                                ),
                                width: MediaQuery.of(context).size.width*0.8,
                                child: Text(i[3], style: TextStyle(fontSize: 24.0, color: Colors.white, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),),
                              onPressed: () {
                                if (i[4] != "4") {
                                  print("a");
                                  setState(() {
                                    carouselController.nextPage(duration: Duration(milliseconds: 200), curve: Curves.ease);
                                  });
                                } else {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => new SignupPage()));
                                }
                              },
                            ),
                          ],
                        ),
                      ),),
                  ],
                ),
              );
            },
          );
        }).toList(),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    _setupCarousel();
    return Scaffold(
      body: Center(
        child: carouselCtrl,
      ),
    );
  }
}
