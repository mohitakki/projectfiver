import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'signup.dart';
import 'package:warmie/main.i18n.dart';
import '../feed.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool showPassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 10)),
                new Text("Login",
                    style: TextStyle(
                        color: Color(0xFF2C666E),
                        fontSize: 32,
                        fontWeight: FontWeight.w600)),
                Padding(padding: EdgeInsets.only(top: 10)),
                new Text(
                  "Buy and sell beautiful things \nin your neighbourhood.".i18n,
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                      fontSize: 16,
                      fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center,
                ),

                /*new FlatButton(
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF4D67AD),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(
                  "Continue with Facebook",
                  style: TextStyle(fontSize: 24.0, color: Colors.white, fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () {},
            ),
            Padding(padding: EdgeInsets.only(top: 10)),
            new FlatButton(
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF5383EC),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(
                  "Continue with Google",
                  style: TextStyle(fontSize: 24.0, color: Colors.white, fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () {},
            ),*/
                Padding(padding: EdgeInsets.only(top: 10)),
                new Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.1,
                      right: MediaQuery.of(context).size.width * 0.1,
                      top: 10),
                  child: new TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                      hintText: "Email address".i18n,
                      border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.1,
                      right: MediaQuery.of(context).size.width * 0.1,
                      top: 10),
                  child: new TextFormField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      hintText: "Password",
                      border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    obscureText: showPassword,
                  ),
                ),
                FlatButton(
                  child: new Text(
                    showPassword == true
                        ? "Show password".i18n
                        : "Hide password".i18n,
                    style: TextStyle(
                        color: Colors.cyan, fontWeight: FontWeight.w500),
                  ),
                  onPressed: () {
                    setState(() {
                      showPassword == false
                          ? showPassword = true
                          : showPassword = false;
                    });
                  },
                ),
                new FlatButton(
                  child: Container(
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xFFE3E8EB),
                    ),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Text(
                      "Login",
                      style: TextStyle(
                          fontSize: 18.0,
                          color: Color(0xFF2C666E),
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  onPressed: () async {
                    try {
                      AuthResult ar = await FirebaseAuth.instance
                          .signInWithEmailAndPassword(
                              email: emailController.text,
                              password: passwordController.text);


                      if (ar.user != null) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FeedPage()));
                      }
                    } catch (e) {
                      Fluttertoast.showToast(msg: e.message);
                    }
                  },
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text("Don't have an account?  ".i18n,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500)),
                    FlatButton(
                      child: Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Color(0xFFE3E8EB),
                        ),
                        width: MediaQuery.of(context).size.width * 0.30,
                        child: Text(
                          "Sign up".i18n,
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Color(0xFF2C666E),
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new SignupPage()));
                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
