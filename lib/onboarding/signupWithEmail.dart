import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:warmie/onboarding/signup.dart';
import 'package:warmie/main.i18n.dart';
import '../feed.dart';

//import 'package:carousel_slider/carousel_slider.dart';
//import 'login.dart';


class SignupWithEmailPage extends StatefulWidget {
  SignupWithEmailPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignupWithEmailPageState createState() => _SignupWithEmailPageState();
}

class _SignupWithEmailPageState extends State<SignupWithEmailPage> {
  final emailController = TextEditingController();
  final nameController = TextEditingController();
  //final phoneController = TextEditingController();
  //bool hidePhone = false;
  final passwordController = TextEditingController();
  //final confirmPasswordController = TextEditingController();
  FocusNode _focusNode = new FocusNode(); //1 - declare and initialize variable

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 10)),
              new Align(
                alignment: Alignment.topLeft,
                child: new IconButton(
                  icon: new Icon(Icons.arrow_back_ios),
                  color: Colors.grey,
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => new SignupPage()));
                  },
                ),
              ),
              //-------------------------------------------------- CREATE ACCOUNT TEXT --------------------------------------------------------
              Padding(padding: EdgeInsets.only(top: 70)),
              new Text("Create Your Account".i18n, style: TextStyle(color: Color(0xFF2C666E), fontSize: 24, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
              Padding(padding: EdgeInsets.only(top: 10)),
              //-------------------------------------------------- NAME --------------------------------------------------------
              new Container(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
                child: new TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    hintText: "Your Name".i18n,
                    border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(
                      ),
                    ),
                  ),
                ),
              ),
             //--------------------------------------------------- EMAIL --------------------------------------------------------
             new Container(
               padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
               child: new TextFormField(
                 controller: emailController,
                 decoration: InputDecoration(
                   hintText: "Your Email".i18n,
                   border: OutlineInputBorder(
                     borderRadius: new BorderRadius.circular(25.0),
                     borderSide: new BorderSide(
                     ),
                   ),
                 ),
               ),
             ),
              //------------------------------------------------- PASSWORD -------------------------------------------------------
              new Container(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
                child: new TextFormField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    hintText: "Password",
                    border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
              ),
              //-------------------------------------------------- JOIN US BUTTON --------------------------------------------------------
              Padding(padding: EdgeInsets.only(top: 20)),
              new FlatButton(
                child: Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Color(0xFFE3E8EB),
                  ),
                  width: MediaQuery.of(context).size.width*0.8,
                  child: Text("Join Us".i18n, style: TextStyle(fontSize: 18.0, color: Color(0xFF2C666E), fontWeight: FontWeight.w500), textAlign: TextAlign.center,),),
                onPressed: _register,
              ),
              //-------------------------------------------------- AGREE --------------------------------------------------------
              Padding(padding: EdgeInsets.only(top: 30)),
              new Text("By pressing 'Join Us' \nyou agree to our terms & conditions".i18n, style: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 16, fontWeight: FontWeight.w700), textAlign: TextAlign.center,),
            ],
          ),
        ),
      ),
    );
  }
  //-------------------------------------------------- CHECKS --------------------------------------------------------
  void _register() async {
    //Check if the email last character is an empty space if so, delete the last char
    String checkLastCharEmail = emailController.text;
    print("last character: " +
        checkLastCharEmail.substring(checkLastCharEmail.length - 1));

    if(checkLastCharEmail.substring(checkLastCharEmail.length - 1) == " ")
    {
      String changeE = emailController.text;
      emailController.text = changeE.substring(0, changeE.length - 1);
    }
    //Check if the name last character is an empty space if so, delete the last char
    String checkLastCharName = nameController.text;
    print("last character: " +
        checkLastCharName.substring(checkLastCharName.length - 1));

    if(checkLastCharName.substring(checkLastCharName.length - 1) == " ")
    {
      String changeN = nameController.text;
      nameController.text = changeN.substring(0, changeN.length - 1);
    }
    //if(passwordController != confirmPasswordController){
      try {
        AuthResult ar = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: emailController.text, password: passwordController.text);



        if (ar.user != null) {
          Firestore.instance.collection('users').add({
            "name": nameController.text,
            "email": emailController.text,
            "avatarURL": "https://firebasestorage.googleapis.com/v0/b/warmie-9607c.appspot.com/o/ProfileImage.gif?alt=media&token=06f941ef-2bc1-4590-baf5-1b7d2d79eb3c",
            "trees": 0.01,
            "userSiteUrl": "Your Website..",
            "prof": "no",
            "call": "Your Phone Number..",
            "companyDescription": "Describe Your Company..",
            "address": "Your Address..",
            "companySlogan":"Your Company Slogan..",
            "numberOfEmployees": "0",
            "yearsOfExperience": "0",
            "CAP": "00000",
            "personalPhoneNumber": "Your Salesman Number..",
            "shippingCosts": "0",
            "instagramLink": "Your instagram link..",
            "facebookLink": "Your facebook link..",
            "whatsappLink": "Your whatsapp link..",
            "questionsEmail": "Your questions email..",
            "pushToken":null,
            "chattingWith":null,
            "uid": ar.user.uid,
          });

          Navigator.push(context, MaterialPageRoute(builder: (context) => new FeedPage()));
        }
      } catch (e) {
        Fluttertoast.showToast(msg: e.message, timeInSecForIos: 3);
      }
      //else{Fluttertoast.showToast(msg:"the password don't match", timeInSecForIos: 3);}
  }
}
