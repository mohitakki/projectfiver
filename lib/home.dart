import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:warmie/clothesSelectType.dart';
import 'package:warmie/filteredSearch.dart';
import 'main.i18n.dart';
import 'item/item.dart';
import 'admob_service.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ams = AdMobService(); //this is used for the ads in the app
  final searchController = TextEditingController();
  Map<String, String> search;

  get tuser => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Color(0xFF58A9B1),
      ),
      child: Column(
        children: <Widget>[
          SafeArea(
            // ------------------------------ TOP SEARCH ------------------------------
            child: Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(0),
              child: Image.asset(
                "assets/onlyLogo.png",
                height: 50,
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.8,
              ),
            ),
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    color: Color(0xFF58A9B1),
                    padding: EdgeInsets.only(left:30.0, right: 30.0,bottom: 5.0, top: 10),
                    width: MediaQuery.of(context).size.width,
                    child: Text("Find Something Unique.".i18n,textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16,fontWeight: FontWeight.w600),),
                  ),
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(left:30.0, right: 30.0,bottom: 30.0,),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height - 180,
                    child: GridView.count(
                      crossAxisCount: 3,
                      primary: false,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 5.0,
                      childAspectRatio: 0.8,
                      children: <Widget>[
                        _buildcard('Clothes'.i18n, 'assets/tshirt.png',context,0),
                        _buildcard('Electronics'.i18n, 'assets/smartphone.png',context,1),
                        _buildcard('Cars'.i18n, 'assets/cars.png',context,2),
                        _buildcard('Books'.i18n, 'assets/books.png',context,3),
                        _buildcard('On Sale'.i18n, 'assets/house.png',context,4),
                        _buildcard('For Rent'.i18n, 'assets/rent.png',context,5),
                        _buildcard('Garden'.i18n, 'assets/garden.png',context,6),
                        _buildcard('Art'.i18n, 'assets/art.png',context,7),
                        _buildcard('Bikes'.i18n, 'assets/bikes.png',context,8),
                        _buildcard('Motorbikes'.i18n, 'assets/motorbike.png',context,9),
                        _buildcard('Animals'.i18n, 'assets/pets.png',context,10),
                        _buildcard('Games'.i18n, 'assets/consoles.png',context,11),
                        _buildcard('Job'.i18n, 'assets/jobs.png',context,12),
                        _buildcard('Sports'.i18n, 'assets/sports.png',context,13),
                        _buildcard('Children'.i18n, 'assets/children.png',context,14),
                        _buildcard('Services'.i18n, 'assets/services.png',context,15),
                        _buildcard('Others'.i18n, 'assets/others.png',context,16),
                      ],
                    ),
                  )
                ],
              ),
          ],
          ),
        ],
      ),
    );
  }
}

//----------------------------------------------------------------------------------------------------

//----------------------------------- Cards of the sell page -----------------------------------------

//----------------------------------------------------------------------------------------------------

Widget _buildcard(String category, String imagePath, context, int idCategory){
  return Padding(
    padding: EdgeInsets.only(top: 15.0, bottom: 5.0, left: 5.0, right: 5.0),
    child: InkWell(
        onTap: () {
          if(idCategory==0)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new ClothesSelectTypePage()));
          }
          if(idCategory==1)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Elettronica".i18n, type:"select", showBack: true,)));
          }
          if(idCategory==2)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Macchine & Accessori".i18n, type:"select", showBack: true,)));
          }
          if(idCategory==3)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Film, Libri e Musica".i18n, type:"select", showBack: true,)));
          }
          if(idCategory==4)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Immobili In Vendita".i18n, type:"select", showBack: true,)));
          }
          if(idCategory==5)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Immobili In Affitto".i18n, type:"select", showBack: true,)));
          }
          if(idCategory==6)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Casa & Giardino".i18n, type:"select", showBack: true,)));
          }
          if(idCategory==7)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Collezionabili & Arte".i18n,  type:"select",showBack: true,)));
          }
          if(idCategory==8)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Biciclette & Accessori".i18n,  type:"select",showBack: true,)));
          }
          if(idCategory==9)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Moto & Accessori".i18n,  type:"select",showBack: true,)));
          }
          if(idCategory==10)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Animali & Accessori".i18n,  type:"select",showBack: true,)));
          }
          if(idCategory==11)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Giochi & Console".i18n,  type:"select",showBack: true,)));
          }
          if(idCategory==12)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Offerte Di Lavoro".i18n,  type:"select",showBack: true,)));
          }
          if(idCategory==13)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Sport, Tempo Libero".i18n, showBack: true,)));
          }
          if(idCategory==14)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Infanzia & Bambini".i18n, showBack: true,)));
          }
          if(idCategory==15)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Servizi".i18n, showBack: true,)));
          }
          if(idCategory==16)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Altro".i18n, showBack: true,)));
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 3.0,
                blurRadius: 5.0,
              )
            ],
            color: Colors.white,
          ),
          child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[

                        ]
                    )
                ),
                //-------------------------------- Category Image -------------------------------
                Hero(
                    tag: imagePath,
                    child: Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(imagePath),
                                fit: BoxFit.contain
                            )
                        )
                    )
                ),
                //------------------------------------- Text ----------------------------------
                SizedBox(height: 7.0),
                Text(category,
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                    )),
              ]
          ),
        )
    ),
  );
}