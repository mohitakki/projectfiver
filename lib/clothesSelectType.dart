import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:warmie/filteredSearch.dart';
import 'main.i18n.dart';
import 'item/item.dart';
import 'admob_service.dart';


class ClothesSelectTypePage extends StatefulWidget {
  @override
  _ClothesSelectTypePageState createState() => _ClothesSelectTypePageState();
}

class _ClothesSelectTypePageState extends State<ClothesSelectTypePage> {
  final ams = AdMobService(); //this is used for the ads in the app
  final searchController = TextEditingController();
  Map<String, String> search;

  get tuser => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Color(0xFF58A9B1),
      ),
      child: Column(
        children: <Widget>[
          AppBar(
            backgroundColor: Color(0xFF58A9B1),
            automaticallyImplyLeading: true,
            title: Text("Choose your Clothes".i18n),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(left:30.0, right: 30.0,bottom: 30.0,),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height - 100,
                    child: GridView.count(
                      crossAxisCount: 3,
                      primary: false,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 5.0,
                      childAspectRatio: 0.8,
                      children: <Widget>[
                        _buildcard('All'.i18n, 'assets/others.png',context,0),
                        _buildcard('Accessories'.i18n, 'assets/accessories.png',context,1),
                        _buildcard('T-Shirt'.i18n, 'assets/tshirt.png',context,15),
                        _buildcard('Hats'.i18n, 'assets/hats.png',context,3),
                        _buildcard('Swimwear'.i18n, 'assets/swimwear.png',context,4),
                        _buildcard('Sweatshirt'.i18n, 'assets/sweatshirts.png',context,5),
                        _buildcard('Jackets'.i18n, 'assets/jackets.png',context,17),
                        _buildcard('Jewelry'.i18n, 'assets/jewelry.png',context,18),
                        _buildcard('Skirts'.i18n, 'assets/skirt.png',context,6),
                        _buildcard('Underwear'.i18n, 'assets/underwear.png',context,7),
                        _buildcard('Glasses'.i18n, 'assets/sunglasses.png',context,8),
                        _buildcard('Watches'.i18n, 'assets/watches.png',context,9),
                        _buildcard('Trousers'.i18n, 'assets/trousers.png',context,10),
                        _buildcard('Wallets'.i18n, 'assets/wallets.png',context,11),
                        _buildcard('Shoes'.i18n, 'assets/shoes.png',context,12),
                        _buildcard('Shorts'.i18n, 'assets/shorts.png',context,13),
                        _buildcard('Tops'.i18n, 'assets/top.png',context,14),
                        _buildcard('Handbags'.i18n, 'assets/handbags.png',context,2),
                        _buildcard('Dresses'.i18n, 'assets/dress.png',context,16),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

//----------------------------------------------------------------------------------------------------

//----------------------------------- Cards of the sell page -----------------------------------------

//----------------------------------------------------------------------------------------------------

Widget _buildcard(String category, String imagePath, context, int idCategory){
  return Padding(
    padding: EdgeInsets.only(top: 15.0, bottom: 5.0, left: 5.0, right: 5.0),
    child: Material(
      child: InkWell(
        onTap: () {
          if(idCategory==0)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori", type:"select", showBack: true,)));
          }
          if(idCategory==1)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n, type:"Accessori", showBack: true,)));
          }
          if(idCategory==2)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n, type:"Borse", showBack: true,)));
          }
          if(idCategory==3)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n, type:"Cappelli", showBack: true,)));
          }
          if(idCategory==4)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n, type:"Costumi", showBack: true,)));
          }
          if(idCategory==5)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n, type:"Felpe", showBack: true,)));
          }
          if(idCategory==6)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n, type:"Gonne", showBack: true,)));
          }
          if(idCategory==7)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Intimo",showBack: true,)));
          }
          if(idCategory==8)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Occhiali",showBack: true,)));
          }
          if(idCategory==9)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Orologi",showBack: true,)));
          }
          if(idCategory==10)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Pantaloni",showBack: true,)));
          }
          if(idCategory==11)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Portafogli",showBack: true,)));
          }
          if(idCategory==12)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Scarpe",showBack: true,)));
          }
          if(idCategory==13)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Shorts", showBack: true,)));
          }
          if(idCategory==14)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Top", showBack: true,)));
          }
          if(idCategory==15)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Magliette", showBack: true,)));
          }
          if(idCategory==16)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Vestiti", showBack: true,)));
          }
          if(idCategory==17)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Giacche", showBack: true,)));
          }
          if(idCategory==18)
          {
            Navigator.push(context, MaterialPageRoute(builder: (context) => new FilteredSearchPage(filter: "Vestiti & Accessori".i18n,  type:"Gioielli", showBack: true,)));
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 3.0,
                blurRadius: 5.0,
              )
            ],
            color: Colors.white,
          ),
          child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[

                        ]
                    )
                ),
                //-------------------------------- Category Image -------------------------------
                Hero(
                    tag: imagePath,
                    child: Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(imagePath),
                                fit: BoxFit.contain
                            )
                        )
                    )
                ),
                //------------------------------------- Text ----------------------------------
                SizedBox(height: 7.0),
                Text(category,
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                    )),
              ]
          ),
        )
    ),),
  );
}