import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:warmie/profile.dart';
import 'item/item.dart';
import 'main.i18n.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class Conversation extends StatefulWidget {
  Conversation(
      {Key key,
      this.documentID,
      this.title,
      this.avatarURL,
      this.item,
      this.us})
      : super(key: key);

  final String documentID;
  final String title;
  final String avatarURL;
  final int us;
  final Map<String, dynamic> item;

  @override
  _ConversationState createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {
  final messageController = TextEditingController();

  get documentID => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        title: Row(
          children: <Widget>[
            GestureDetector(
              onTap: () async {},
              child: CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(
                  widget.avatarURL,
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(10)),
            Text(widget.title),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_horiz),
            onPressed: () {
              showCupertinoModalPopup(
                  context: context,
                  builder: (BuildContext context) {
                    return CupertinoActionSheet(
                      actions: <Widget>[
                        CupertinoActionSheetAction(
                          child: Text(
                            "Block User".i18n,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context, "delete");
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: Text(
                            "Report the user".i18n,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.red,
                            ),
                          ),
                          onPressed: () async {
                            Navigator.pop(context);
                            await Firestore.instance
                                .collection('inbox')
                                .document(widget.documentID)
                                .setData({"reported": true}, merge: true);
                            Fluttertoast.showToast(msg: "User reported.");
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: Text(
                            "Delete Chat".i18n,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.red,
                            ),
                          ),
                          onPressed: () async {
                            Navigator.pop(context);
                            Navigator.pop(context, "delete");
                          },
                        ),
                      ],
                      cancelButton: CupertinoActionSheetAction(
                        child: Text(
                          "Cancel".i18n,
                          style: TextStyle(
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    );
                  });
            },
          ),
        ],
      ),
      //--------------------------------------------------- TOP CHAT ITEM SECTION --------------------------------------
      //                                This is the script to see the item on the top of the chat
      //----------------------------------------------------------------------------------------------------------------
      body: Column(
        children: <Widget>[
          Container(
            height: 60,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[300],
                  spreadRadius: 4,
                  blurRadius: 8,
                ),
              ],
            ),
            padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  onTap: () async {
                    var item;
                    QuerySnapshot qs = await Firestore.instance
                        .collection('inbox')
                        .where('item.docRef', isEqualTo: item)
                        .getDocuments();
                    var v;
                    v.data["documentID"] = qs;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new ViewListing(item: v)));
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: CachedNetworkImageProvider(widget.item["image"]),
                      ),
                    ),
                  ),
                ),
                new Text(
                  widget.item["title"],
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                ),
                new Text(
                  widget.item["price"].toString() + " €",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 16,
                      color: Color(0xFF58A9B1)),
                ),
              ],
            ),
          ),
          Expanded(
            child:
                //--------------------------------------- ALL THE MESSAGES IN THE CHAT ------------------------------
                //                      This is the script to see all the messages between the 2 users
                //---------------------------------------------------------------------------------------------------
                StreamBuilder(
              stream: Firestore.instance
                  .collection('inbox')
                  .document(widget.documentID)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return CircularProgressIndicator();
                }

                return new ListView(
                  reverse: true,
                  children: snapshot.data["messages"]
                      .map<Widget>((v) {
                        /*return ListTile(
                        title: Text(snapshot.data["userData"][v["sender"]]["name"]),
                        subtitle: Text(v["body"]),
                        leading: CircleAvatar(backgroundImage: NetworkImage(snapshot.data["userData"][v["sender"]]["avatarURL"])),
                      );*/
                        return Container(
                          margin: widget.us == v["sender"]
                              ? EdgeInsets.only(
                                  right: 10,
                                  left: MediaQuery.of(context).size.width / 3.5,
                                  bottom: 10,
                                )
                              : EdgeInsets.only(
                                  left: 10,
                                  right:
                                      MediaQuery.of(context).size.width / 3.5,
                                  bottom: 10,
                                ),
                          decoration: BoxDecoration(
                            color: widget.us == v["sender"]
                                ? Color(0xFF58A9B1)
                                : Colors.white, //border:Border.all(),
                            borderRadius: widget.us == v["sender"]
                                ? BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                    bottomRight: Radius.circular(0),
                                    bottomLeft: Radius.circular(20))
                                : BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                    bottomLeft: Radius.circular(0),
                                    bottomRight: Radius.circular(20)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[300],
                                spreadRadius: 4,
                                blurRadius: 8,
                              ),
                            ],
                          ),
                          padding: EdgeInsets.all(20),
                          child: Text(v["body"],
                              style: TextStyle(
                                color: widget.us == v["sender"]
                                    ? Colors.white
                                    : Colors.grey[800],
                                fontWeight: FontWeight.w600,
                              )),
                        );
                      })
                      .toList()
                      .reversed
                      .toList(),
                );
              },
            ),
          ),

          //---------------------------------------------- WRITE A MESSAGE BOX ---------------------------------------------
          Container(
            height: 90,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[300],
                  spreadRadius: 4,
                  blurRadius: 8,
                ),
              ],
            ),
            padding: EdgeInsets.all(5),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(bottom: 10),
                  width: MediaQuery.of(context).size.width - 100,
                  child: new TextFormField(
                    controller: messageController,
                    textInputAction: TextInputAction.send,
                    onFieldSubmitted: (v) {},
                    textCapitalization: TextCapitalization.sentences,
                    decoration: InputDecoration(
                      hintText: "write your message...".i18n,
                      border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    maxLines: 1,
                  ),
                ),
                //------------------------------------------- SEND MESSAGE BUTTON --------------------------------------------
                new Material(
                  color: Colors.white,
                  child: Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Ink(
                      decoration: const ShapeDecoration(
                        color: Color(0xFF58A9B1),
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                          icon: Icon(Icons.send, color: Colors.white),
                          onPressed: () async {
                            //ekhane uid pathate hobe

                            var userLog;
                            var reciver;

                            Firestore.instance
                                .collection("notifications")
                                .document(widget.item[""])
                                .get()
                                .then((value) {
                              userLog = value.data["users"][0];
                              reciver = value.data["users"][1];
                            });





                            Firestore.instance
                                .collection('users')
                                .where('email', isEqualTo: userLog)
                                .getDocuments()
                                .then((querySnapshot) {
                              querySnapshot.documents
                                  .forEach((documentSnapshot) {
                                if (documentSnapshot.data["uid"] == userLog) {
                                  documentSnapshot.reference
                                      .updateData({"extra1": reciver});
                                }
                              });
                            });

                            if (messageController.text == "") {
                            } else {
                              await Firestore.instance
                                  .collection('inbox')
                                  .document(widget.documentID)
                                  .setData({
                                'messages': FieldValue.arrayUnion(
                                  [
                                    {
                                      "body": messageController.text,
                                      "sender": widget.us,
                                      "sent":
                                          DateTime.now().millisecondsSinceEpoch,
                                    },
                                  ],
                                ),
                                'seen': [],
                              }, merge: true);
                              setState(() {
                                messageController.text = "";
                              });
                            }
                          }),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
