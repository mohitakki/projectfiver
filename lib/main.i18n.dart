import 'package:i18n_extension/i18n_extension.dart';

/*

    This is the Translation file, this file contains ALL
    the translation of the app.

    This is really simple, to add something new just add this:

    {
        "en_us": "Text in english",
        "it" : "Text in italian"
    }+

    If you need help on how to use this file,
    Check this link:
    https://medium.com/flutter-community/i18n-extension-flutter-b966f4c65df9

 */


extension Localization on String {

  static var _t = Translations("en_us") +
      //-------------------------- INTRODUCTION PAGE TRANSLATIONS
      {
        "en_us": "Get Started",
        "it" : "Inizia"
      }+
      {
        "en_us": "Next",
        "it" : "Prossimo"
      }+
      {
        "en_us": " of 4",
        "it" : " di 4"
      }+
      {
        "en_us": "You earn, We Plant trees.",
        "it" : "Tu guadagni, Noi Piantiamo alberi."
      }+
      {
        "en_us": "With this app we want to help fight \n global warming and climate change.",
        "it" : "Con quest'app vogliamo aiutare a fermare \n il riscaldamento globale."
      }+
      {
        "en_us": "Buy and Sell",
        "it" : "Compra e Vendi"
      }+
      {
        "en_us": "You buy and sell things \n and you take all the money.",
        "it" : "Tu compri e vendi oggetti \n e tieni tutti i guadagni."
      }+
      {
        "en_us": "How we earn money",
        "it" : "Come guadagnamo"
      }+
      {
        "en_us": "In your search there are ads, \n from these ads we generate revenue.",
        "it" : "Nelle tue ricerche ci sono pubblicità \n da queste, generiamo un guadagno."
      }+
      {
        "en_us": "Circular Economy",
        "it" : "Economia Circolare"
      }+
      {
        "en_us": "By reusing used things we can reduce the \n CO2 emissions caused by their \n production and reduce waste.",
        "it" : "Riutilizzando le cose usate possiamo ridurre \n le emissioni di CO2 causate \n dalla produzione e ridurre gli sprechi."
      }+
      {
        "en_us": "We plant trees",
        "it" : "Noi piantiamo alberi"
      }+
      {
        "en_us": "We plant trees and in doing so \n we reduce CO2 emissions and help \n reduce the advance of global warming.",
        "it" : "Piantiamo alberi e così facendo \n riduciamo il CO2 emesso e aiutiamo \n a ridurre il riscaldamento globale."
      }+
      {
        "en_us": "Sign up",
        "it" : "Registrati"
      }+
      {
        "en_us": "Continue with Email",
        "it" : "Continua con l'email"
      }+
      {
        "en_us": "Already a member?  ",
        "it" : "Già iscritto?"
      }+
      {
        "en_us": "Login",
        "it" : "Login"
      }+
      {
        "en_us": "Create Your Account",
        "it" : "Crea il tuo Account"
      }+
      {
        "en_us": "Your Name",
        "it" : "Il Tuo Nome"
      }+
      {
        "en_us": "Your Email",
        "it" : "La Tua Email"
      }+
      {
        "en_us": "Join Us",
        "it" : "Registrati"
      }+
      {
        "en_us": "By pressing 'Join Us' \nyou agree to our terms & conditions",
        "it" : "Premendo 'Registrati' \naccetti i nostri termini & condizioni"
      }+
      {
        "en_us": "Don't have an account?  ",
        "it" : "Non hai un account?"
      }+
      {
        "en_us": "Show password",
        "it" : "Mostra password"
      }+
      {
        "en_us": "Hide password",
        "it" : "Nascondi password"
      }+
      {
        "en_us": "Email address",
        "it" : "Indirizzo Email"
      }+
      {
        "en_us": "Buy and sell beautiful \nthings and help the world :D",
        "it" : "Compra e vendi oggetti \n stupendi e aiuta il mondo :D"
      }+
      {
        "en_us": "Buy and sell beautiful things \nin your neighbourhood.",
        "it" : "Compra e vendi oggetti stupendi \nnel tuo vicinato."
      }+
      {
        "en_us": "",
        "it" : ""
      }+
      {
        "en_us": "",
        "it" : ""
      }+
      {
        "en_us": "",
        "it" : ""
      }+
      //--------------------------- PROFILE PAGE TRANSLATIONS -----------------
      {
        "en_us": "You Have Planted:",
        "it" : "Hai Piantato:"
      }+
      {
        "en_us":"There's 0 item on sale \n Try to sell something \n You don't use anymore",
        "it":"Ci sono 0 Oggetti in vendita \n Prova a vendere qualcosa \n Che non usi più",
      }+
      {
        "en_us": "There's nothing here! \n Try to sell something",
        "it" : "Non c'è niente qui! \nProva a vendere qualcosa",
      }+
      //--------------------------- SETTINGS PAGE TRANSLATIONS -----------------
      {
        "en_us": "Settings",
        "it" : "Impostazioni",
      }+
      {
        "en_us": "Change Profile Picture",
        "it" : "Cambia Immagine Profilo",
      }+
      {
        "en_us": "Change Language",
        "it" : "Cambia Lingua",
      }+
      {
        "en_us": "Privacy Policy",
        "it" : "Politica sulla Riservatezza",
      }+
      {
        "en_us": "Terms Of Use",
        "it": "Termini d'utilizzo",
      }+
      {
        "en_us": "FAQ",
        "it" : "Domande Frequenti",
      }+
      {
        "en_us": "Call: ",
        "it": "Chiama: ",
      }+
      {
        "en_us": "Contact Us",
        "it" : "Contattaci",
      }+
      {
        "en_us": "Warmie app : Version 1.0.21",
        "it" : "Warmie app : Versione 1.0.21",
      }+
      {
        "en_us": "Edit Profile",
        "it": "Modifica Profilo",
      }+
      {
        "en_us": "Change Profile Image",
        "it": "Cambia Immagine Profilo",
      }+
      {
        "en_us": "Profile Name",
        "it": "Nome Profilo",
      }+
      {
        "en_us": "Company Description",
        "it": "Descrizione dell'azienda",
      }+
      {
        "en_us": "Address",
        "it": "Indirizzo",
      }+
      {
        "en_us": "Other Info",
        "it": "Altre Informazioni",
      }+
      {
        "en_us": "Number Of Employees",
        "it": "Numero di Dipendenti",
      }+
      {
        "en_us": "Years Of Experience",
        "it": "Anni d'esperienza",
      }+
      {
        "en_us": "More Info",
        "it": "Più Info",
      }+
      {
        "en_us": "Office Number",
        "it": "Numero dell'ufficio",
      }+
      {
        "en_us": "Enter your website url",
        "it": "Inserisci l'indirizzo del tuo sito web",
      }+
      {
        "en_us": "Enter your phone number",
        "it": "Inserisci il numero dell'ufficio",
      }+
      {
        "en_us": "Company Description",
        "it": "Descrizione dell'azienda",
      }+
      {
        "en_us": "Enter your company description..",
        "it": "Inserisci la descrizione della tua azienda..",
      }+
      {
        "en_us": "Enter your company address..",
        "it": "Inserisci l'indirizzo della tua azienda..",
      }+
      {
        "en_us": "Slogan of the company",
        "it": "Slogan Dell'azienda",
      }+
      {
        "en_us": "Enter max 100 characters..",
        "it": "Inserisci massimo 100 caratteri..",
      }+
      {
        "en_us": "Personal Number",
        "it": "Numero Personale",
      }+
      {
        "en_us": "Enter salesman number..",
        "it": "Inserisci numero personale..",
      }+
      {
        "en_us": "Shipping Costs",
        "it": "Costi di spedizione",
      }+
      {
        "en_us": "Email for Questions",
        "it": "Email per le Domande",
      }+
      {
        "en_us": "Useful Information",
        "it": "Informazioni Utili",
      }+
      {
        "en_us": "Telegram Group",
        "it": "Gruppo Telegram",
      }+
      {
       "en_us": "- Avoid scams by acting locally or by paying with PayPal Never pay with Western Union, Moneygram or other anonymous payment services \n \n - Don't buy or sell outside your country. Don't accept cashiers checks outside your country \n \n - This app is not involved in any transaction and does not handle payments, purchases, guarantee transactions, electronic payment services, or buyer protection or sales certificates offers.",
       "it": "- Evitate truffe agendo a livello locale o pagando con PayPal Non pagare mai con Western Union, Moneygram o altri servizi di pagamento anonimi \n \n - Non acquistare o vendere al di fuori del proprio paese. \n \n - Non accettare assegni circolari fuori dal tuo paese Questa app non è coinvolta in nessuna transazione e non gestisce pagamenti, acquisti, le transazioni di garanzia, servizi di pagamento elettronici, o offerte di protezione dell'acquirente o certificati di vendita",
      }+
      {
        "en_us": "Number Of Employees: ",
        "it": "Numero Di Dipendenti: ",
      }+
      {
        "en_us": "Years Of Experience: ",
        "it": "Anni D'esperienza: ",
      }+
      {
        "en_us": "Number of employees",
        "it": "Numero di impiegati",
      }+
      {
        "en_us": "Years of experience",
        "it":"Anni d'esperienza",
      }+
      {
        "en_us":  "If you don't change the default value of a field it will not be displayed",
        "it": "Se non cambi il valore di default di un campo, non verrà mostrato",
      }+
      {
        "en_us": "Enter shipping price..",
        "it": "Inserisci i costi di spedizione..",
      }+
      {
        "en_us": "Unlock PRO Features",
        "it": "Sblocca le Funzionalità PRO",
      }+
      {
        "en_us": "Unlock PRO Features in Settings > Become a PRO",
        "it": "Sblocca le Funzionalità PRO in Impostazioni > Diventa un PRO",
      }+
      {
        "en_us": "Recurring monthly payment, that you can cancel at any time. If you decide to purchase a subscription, the payment will be charged to your iTunes account. The amount will be charged to your account within 24 hours before the end of the current validity period. Once the purchase is made, you can deactivate The automatic renewal at any time from iTunes settings. If you want More information visit our Terms and conditions and the Privacy Policy In profile>settings>others",
        "it": "Pagamento mensile riccorrente, che puoi cancellare in qualsiasi momento. Se decidi di comprare un iscrizione, il pagamento sarà effettuato sul tuo account iTunes. La somma verrà pagata nelle 24 ore prima della fine del periodo di validità. Una volta che l'acquisto è stato fatto, puoi disattivare il rinnovo automatico in ogni momento dalle impostazioni di iTunes. Se vuoi altre informazioni visita i nostri termini e condizioni e la politica sulla riservatezza in impostazioni>altro",
      }+
      {
        "en_us": "1) A special place in our PRO page (future)\n2) A personalized page for you \n3) The ability to add website to your page \n4) The ability to add phone to your page \n5) All day support via email and phone \n6) Find more clients \n7) Your listings won’t expire \n8) Cancel subscription at any time \n9) Add Opening Hours (future) \n10) Add details about you (future) \n11)Receive the new feature before the rest",
        "it": "1) Un speciale posto nella pagina PRO (in arrivo) \n2) Una pagina personalizzata per te \n3) L'abilità d'aggiungere il tuo sito web \n4) L'abilità d'aggiungere il tuo telefono \n5) Supporto 24/7 via email e telefono \n6) Trova più clienti \n7) I tuoi articoli non scadranno \n8) Cancella l'abbonamento quando vuoi \n9) Aggiungi orari d'apertura (in arrivo)\n10) Aggiungi dettagli su di te (in arrivo)\n11)Ricevi le novità prima degli altri",
      }+
      {
        "en_us": "Subscribe now",
        "it": "Iscriviti adesso",
      }+
      {
        "en_us": "Phone Number",
        "it": "Numero di Telefono",
      }+
      {
        "en_us": "Already Purchased, \n \n If you have need help contact us at: warmie.trees@gmail.com \n \n Thank you :D",
        "it": "Già Acquistato, \n \n Se avete bisogno d'aiuto contattateci all'email: warmie.trees@gmail.com \n \n Grazie mille :D",
      }+
      {
        "en_us": "Become a PRO",
        "it" : "Diventa un PRO",
      }+
      {
        "en_us": "Enter your new name...",
        "it" : "Inserisci il nuovo nome...",
      }+
      {
        "en_us": "\n Settings > Become a PRO",
        "it" : "\n Impostazioni > Diventa un PRO",
      }+
      {
        "en_us": "Website",
        "it": "Sito Web",
      }+
      {
        "en_us": "Call",
        "it": "Chiama",
      }+
      {
        "en_us": "Change profile name",
        "it" : "Cambio nome profilo",
      }+
      {
        "en_us": "Uploading image...",
        "it" : "Sto caricando l'immagine...",
      }+
      {
        "en_us": "Logout",
        "it" : "Esci Dall'app",
      }+
      {
        "en_us": "On Sale",
        "it" : "In Vendita",
      }+
      //--------------------------- LANGUAGE PAGE TRANSLATIONS -----------------
      {
        "en_us": "Language",
        "it" : "Lingua",
      }+
      {
        "en_us": "Tap For Italian",
        "it" : "Clicca per Italiano",
      }+
      {
        "en_us": "Clicca per Italiano",
        "it" : "Clicca per Italiano",
      }+
      {
        "en_us": "Tap for English",
        "it" : "Clicca per l'Inglese",
      }+
      //--------------------------- NEWS PAGE TRANSLATIONS -----------------
      {
        "en_us": "News",
        "it" : "Notizie",
      }+
      {
        "en_us": "Trees planted by Warmie",
        "it" : "Alberi piantati da Warmie",
      }+
      {
        "en_us": "Financial Reports",
        "it" : "I nostri Guadagni",
      }+
      {
        "en_us": "Projects",
        "it" : "Progetti",
      }+
      {
        "en_us": "Collaborations",
        "it" : "Collaborazioni",
      }+
      {
        "en_us": "Support The App",
        "it" : "Supporta L'App",
      }+
      {
        "en_us": "See how is the air in your city",
        "it": "Vedi com'è l'aria nella tua città",
      }+
      //--------------------------- SELL PAGE TRANSLATIONS -----------------
      {
        "en_us": "Sell an Item & Help the World",
        "it" : "Vendi un Oggetto e Salva il Mondo",
      }+
      {
        "en_us": "Clothes",
        "it" : "Vestiti",
      }+
      {
        "en_us": "Electronics",
        "it" : "Elettronica",
      }+
      {
        "en_us": "Cars",
        "it" : "Macchine",
      }+
      {
        "en_us": "Books",
        "it" : "Libri",
      }+
      {
        "en_us": "House",
        "it" : "Case",
      }+
      {
        "en_us": "Services",
        "it" : "Servizi",
      }+
      {
        "en_us": "See All The Categories",
        "it" : "Vedi Tutte Le Categorie",
      }+
      {
        "en_us": "All the categories",
        "it" : "Tutte Le Categorie",
      }+
      //------------------------ ALL CATEGORIES TRANSLATION ----------------
      {
        "en_us": "Categories",
        "it" : "Categorie",
      }+
      {
        "en_us":"All",
        "it":"Tutti",
      }+
      {
        "en_us":"Accessories",
        "it":"Accessori",
      }+
      {
        "en_us":"Handbags",
        "it":"Borse",
      }+
      {
        "en_us":"Hats",
        "it":"Cappelli",
      }+
      {
        "en_us":"Swimwear",
        "it":"Costumi",
      }+
      {
        "en_us":"Sweatshirt",
        "it":"Felpe",
      }+
      {
        "en_us":"Skirts",
        "it":"Gonne",
      }+
      {
        "en_us":"Underwear",
        "it":"Intimo",
      }+
      {
        "en_us":"Glasses",
        "it":"Occhiali",
      }+
      {
        "en_us":"Watches",
        "it":"Orologi",
      }+
      {
        "en_us":"Trousers",
        "it":"Pantaloni",
      }+
      {
        "en_us":"Wallets",
        "it":"Portafogli",
      }+
      {
        "en_us":"Shoes",
        "it":"Scarpe",
      }+
      {
        "en_us":"Shorts",
        "it":"Shorts",
      }+
      {
        "en_us":"Tops",
        "it":"Top",
      }+
      {
        "en_us":"T-Shirt",
        "it":"Magliette",
      }+
      {
        "en_us":"Dresses",
        "it":"Vestiti",
      }+
      {
        "en_us":"Choose your Clothes",
        "it":"Scegli i tuoi Vestiti",
      }+
      {
        "en_us":"Select",
        "it":"Seleziona",
      }+
      {
        "en_us":"Clothes Types",
        "it":"Categorie di Vestiti",
      }+
      {
        "en_us":"Jackets",
        "it":"Giacche",
      }+
      {
        "en_us":"Jewelry",
        "it":"Gioielli",
      }+
      {
        "en_us":"View Online Items",
        "it":"Vedi Gli Oggetti Online",
      }+
      {
        "en_us":"Marked As Sold.",
        "it":"Segnato Come Venduto.",
      }+
      {
        "en_us":"Mark As Sold",
        "it":"Segnalo Come Venduto",
      }+
      {
        "en_us":"Sold",
        "it":"Venduti",
      }+
      {
        "en_us":"Sold.",
        "it":"Venduto",
      }+
      {
        "en_us": "Clothing & Accessories",
        "it" : "Vestiti & Accessori",
      }+
      {
        "en_us": "Home & Garden",
        "it" : "Casa & Giardino",
      }+
      {
        "en_us": "Childhood, Children, Baby",
        "it" : "Infanzia, Bambini",
      }+
      {
        "en_us": "Sports, Freetime",
        "it" : "Sport, Tempo Libero",
      }+
      {
        "en_us": "Games & Consoles",
        "it" : "Giochi & Console",
      }+
      {
        "en_us": "Animals and Accessories",
        "it" : "Animali & Accessori",
      }+
      {
        "en_us": "Movie, Books and Music",
        "it" : "Film, Libri e Musica",
      }+
      {
        "en_us": "Electronics",
        "it" : "Elettronica",
      }+
      {
        "en_us": "Cars & Accessories",
        "it" : "Macchine & Accessori",
      }+
      {
        "en_us": "Vehicles & Accessories",
        "it" : "Veicoli & Accessori",
      }+
      {
        "en_us": "Motorbike & Accessories",
        "it" : "Moto & Accessori",
      }+
      {
        "en_us": "Bikes & Accessories",
        "it" : "Biciclette & Accessori",
      }+
      {
        "en_us": "Property On Sale",
        "it" : "Immobili In Vendita",
      }+
      {
        "en_us": "Property For Rent",
        "it" : "Immobili In Affitto",
      }+
      {
        "en_us": "Collectibles & Art",
        "it" : "Collezionabili & Arte",
      }+
      {
        "en_us": "Jobs",
        "it" : "Offerte Di Lavoro",
      }+
      {
        "en_us": "Services",
        "it" : "Servizi",
      }+
      {
        "en_us": "Others",
        "it" : "Altro",
      }+
      //---------------------------- CHAT PAGE TRANSLATIONS ----------------
      {
        "en_us": "Chats",
        "it" : "Messaggi",
      }+
      {
        "en_us": "write your message...",
        "it" : "Scrivi il tuo messaggio..",
      }+
      {
        "en_us": "I'm interested in this item!",
        "it": "Sono interessato a questo articolo",
      }+
      {
        "en_us": "We'll keep messages for any item you're trying to buy/sell in here",
        "it": "Terremo tutti i messaggi per gli oggetti che provi a comprare/vendere qui",
      }+
      {
        "en_us": "No messages yet?",
        "it": "Ancora nessun messaggio?",
      }+
      {
        "en_us": "",
        "it": "",
      }+
      {
        "en_us": "Block User",
        "it" : "Blocca Utente",
      }+
      {
        "en_us": "Report the user",
        "it" : "Segnala l'utente",
      }+
      {
        "en_us": "Delete Chat",
        "it" : "Cancella Chat",
      }+
      {
        "en_us": "Cancel",
        "it" : "Annulla",
      }+
      //----------------------- SEARCH PAGE TRANSLATIONS --------------------
      {
        "en_us": "Find Cars, Property and more...",
        "it" : "Trova Macchine, Case e altro...",
      }+
      {
        "en_us": "Advanced Research",
        "it" : "Ricerca Avanzata",
      }+
      {
        "en_us": "About every 45 searches we plant a tree",
        "it" : "Circa ogni 45 ricerche piantiamo un albero",
      }+
      //--------------- ITEM PAGE TRANSLATIONS ------------------
      {
        "en_us": "Report item",
        "it" : "Segnala L'oggetto",
      }+
      {
        "en_us": "Details",
        "it" : "Dettagli",
      }+
      {
        "en_us": "Delete",
        "it" : "Cancella",
      }+
      {
        "en_us": "EDIT",
        "it" : "MODIFICA",
      }+
      {
        "en_us": "UPDATE",
        "it" : "AGGIORNA",
      }+
      {
        "en_us": "Update",
        "it": "Aggiorna",
      }+
      //--------------- ADVANCED RESEARCH PAGE TRANSLATIONS --------------
      {
        "en_us": "Location",
        "it" : "Posizione",
      }+
      {
        "en_us": "All the world",
        "it" : "Tutto il mondo",
      }+
      {
        "en_us": "Category",
        "it" : "Categoria",
      }+
      {
        "en_us": "italy",
        "it" : "italia",
      }+
      {
        "en_us": "usa",
        "it" : "Stati Uniti",
      }+
      {
        "en_us": "france",
        "it" : "Francia",
      }+
      {
        "en_us": "england",
        "it" : "Inghilterra",
      }+
      {
        "en_us": "spain",
        "it" : "Spagna",
      }+
      {
        "en_us": "china",
        "it" : "Cina",
      }+
      {
        "en_us": "brazil",
        "it" : "Brasile",
      }+
      {
        "en_us": "japan",
        "it" : "Giappone",
      }+
      {
        "en_us": "germany",
        "it" : "Germania",
      }+
      {
        "en_us": "mexico",
        "it" : "Messico",
      }+
      {
        "en_us": "canada",
        "it" : "Canada",
      }+
      {
        "en_us": "Apply",
        "it" : "Applica",
      }+
      //---------------------- CREATE ITEM PAGE TRANSLATIONS ------------------
      {
        "en_us": "Item Name",
        "it" : "Nome Dell'oggetto",
      }+
      {
        "en_us": "Your Location",
        "it" : "Dove Sei",
      }+
      {
        "en_us": "Price",
        "it" : "Prezzo",
      }+
      {
        "en_us": "Description",
        "it" : "Descrizione",
      }+
      {
        "en_us": "Sell",
        "it" : "Vendi",
      }+
      {
        "en_us": "Uploading Image...",
        "it" : "Sto Caricando L'immagine..",
      }+
      {
        "en_us": "Enter the name of the product...",
        "it" : "Inserisci il nome del prodotto...",
      }+
      {
        "en_us": "Enter your Location.. (ex: city, state)",
        "it" : "Inserisci la tua città.. (es: città, stato)",
      }+
      {
        "en_us": "Enter the Price...",
        "it" : "Inserisci il prezzo..",
      }+
      {
        "en_us": "Enter the Description...",
        "it" : "Inserisci la Descrizione..",
      }+
      {
        "en_us": "Adding Listing...",
        "it" : "Lo stiamo mettendo in vendita...",
      }+
      {
        "en_us": "Congratulations your product \nHas been published!",
        "it" : "Congratulazioni il tuo prodotto \nE' ora in vendita!",
      }+
      {
        "en_us": "Thank you for supporting Warmie \n We are stopping the global warming with your help",
        "it" : "Grazie per supportare Warmie. \n Grazie al tuo aiuto fermeremo il riscaldamento globale",
      }+
      {
        "en_us": "Continue",
        "it" : "Continua",
      }+
      //------------------------ ADMIN PAGE TRANSLATIONS --------------------
      {
        "en_us": "Warmie Admin",
        "it" : "Admin Warmie",
      }+
      {
        "en_us": "Articles on Sale",
        "it" : "Articoli in Vendita",
      }+
      {
        "en_us": "Reported Listings",
        "it" : "Articoli Segnalati",
      }+
      {
        "en_us": "What do you want to do with this listing?",
        "it" : "Cosa vuoi fare con questo oggetto?",
      }+
      {
        "en_us":"For Rent",
        "it":"In Affitto",
      }+
      {
        "en_us":"Garden",
        "it":"Giardino",
      }+
      {
        "en_us":"Art",
        "it":"Arte",
      }+
      {
        "en_us":"Bikes",
        "it":"Biciclette",
      }+
      {
        "en_us":"Motorbikes",
        "it":"Moto",
      }+
      {
        "en_us":"Animals",
        "it":"Animali",
      }+
      {
        "en_us":"Games",
        "it":"Giochi",
      }+
      {
        "en_us":"Sports",
        "it":"Sport",
      }+
      {
        "en_us":"Children",
        "it":"Bambini",
      }+
      {
        "en_us":"Job",
        "it":"Lavoro",
      }+
      {
        "en_us":"Find Something Unique.",
        "it":"Trova Qualcosa Di Unico.",
      }+

      {
        "en_us": "View",
        "it" : "Vedi",
      }+
      {
        "en_us": "Delete",
        "it" : "Cancella",
      }+
      {
        "en_us": "Unreport",
        "it" : "Unreport",
      }+
      {
        "en_us": "Chat",
        "it" : "Messaggi",
      }+
      {
        "en_us": "SELL",
        "it" : "VENDI",
      }+
      {
        "en_us": "Profile Updated.",
        "it": "Profilo Aggiornato.",
      }+
      {
        "en_us": "Search",
        "it" : "Cerca",
      }+
      {
        "en_us": "Profile",
        "it" : "Profilo",
      };

      //------------------------ Don't Delete this string. -------------------

      String get i18n => localize(this, _t);
}