import 'dart:async';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'main.i18n.dart';

final String testID = 'auto_renew_subscription_warmie_monthly';

class PackagePage extends StatefulWidget {
  @override
  PackagePage({@required this.thisUser, this.showBack});

  var thisUser;
  bool showBack;
  _PackagePageState createState() => _PackagePageState();
}

class _PackagePageState extends State<PackagePage> {

  InAppPurchaseConnection _iap = InAppPurchaseConnection.instance;
  bool _available = true;
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];
  StreamSubscription _subscription;

  static var dt = DateTime.now();
  static var newFormat = DateFormat("yy-MM-dd");
  String prof = newFormat.format(dt);


@override
void initState(){
  _initialize();
  super.initState();
}

Future<void> _getProducts() async {
  Set<String> ids = Set.from([testID, 'auto_renew_subscription_warmie_monthly']);
  ProductDetailsResponse response = await _iap.queryProductDetails(ids);

  setState((){
    _products = response.productDetails;
  });
}

Future<void> _getPastPurchases() async {
  QueryPurchaseDetailsResponse response = await _iap.queryPastPurchases();

  for(PurchaseDetails purchase in response.pastPurchases){
    if(Platform.isIOS){
      _iap.completePurchase(purchase);
    }
  }

  setState((){
    _purchases = response.pastPurchases;
  });
}

PurchaseDetails _hasPurchased(String productID, {prod}){
  return _purchases.firstWhere( (purchase) => purchase.productID == productID, orElse: () => null);
}

Future<void> _verifyPurchase() async {
  PurchaseDetails purchase = _hasPurchased(testID);

  if(purchase != null && purchase.status == PurchaseStatus.purchased){
    var dt = DateTime.now();
    var newFormat = DateFormat("yy-MM-dd");
    String prof = newFormat.format(dt);
    QuerySnapshot qs = await Firestore.instance.collection('users').where('uid', isEqualTo: widget.thisUser["uid"]).getDocuments();
    await Firestore.instance.collection('users').document(qs.documents.first.documentID).setData({
      "prof": prof,
    }, merge: true);

    QuerySnapshot qp = await Firestore.instance.collection('prousers').where('uid', isEqualTo: widget.thisUser["uid"]).getDocuments();
    if(qp.documents.length == 0){
      Firestore.instance.collection('prousers').add({
        "name": widget.thisUser["name"],
        "email": widget.thisUser["email"],
        "avatarURL": widget.thisUser["avatarURL"],
        "userSiteUrl": widget.thisUser["userSiteUrl"],
        "prof": prof,
        "call": widget.thisUser["call"],
        "companyDescription": widget.thisUser["companyDescription"],
        "address": widget.thisUser["address"],
        "companySlogan": widget.thisUser["companySlogan"],
        "numberOfEmployees": widget.thisUser["numberOfEmployees"],
        "yearsOfExperience": widget.thisUser["yearsOfExperience"],
        "CAP": widget.thisUser["CAP"],
        "personalPhoneNumber": widget.thisUser["personalPhoneNumber"],
        "shippingCosts": widget.thisUser["shippingCosts"],
        "instagramLink": widget.thisUser["instagramLink"],
        "facebookLink": widget.thisUser["facebookLink"],
        "whatsappLink": widget.thisUser["whatsappLink"],
        "questionsEmail": widget.thisUser["questionsEmail"],
        "pushToken":null,
        "chattingWith":null,
        "uid": widget.thisUser["uid"],
      });
    }
    else {
      QuerySnapshot qp2 = await Firestore.instance.collection('prousers').where('uid', isEqualTo: widget.thisUser["uid"]).getDocuments();
      qp2.documents.forEach((doc) => Firestore.instance.collection('prousers').document(doc.documentID).setData({
        "name": widget.thisUser["name"],
        "email": widget.thisUser["email"],
        "avatarURL": widget.thisUser["avatarURL"],
        "userSiteUrl": widget.thisUser["userSiteUrl"],
        "prof": prof,
        "call": widget.thisUser["call"],
        "companyDescription": widget.thisUser["companyDescription"],
        "address": widget.thisUser["address"],
        "companySlogan": widget.thisUser["companySlogan"],
        "numberOfEmployees": widget.thisUser["numberOfEmployees"],
        "yearsOfExperience": widget.thisUser["yearsOfExperience"],
        "CAP": widget.thisUser["CAP"],
        "personalPhoneNumber": widget.thisUser["personalPhoneNumber"],
        "shippingCosts": widget.thisUser["shippingCosts"],
        "instagramLink": widget.thisUser["instagramLink"],
        "facebookLink": widget.thisUser["facebookLink"],
        "whatsappLink": widget.thisUser["whatsappLink"],
        "questionsEmail": widget.thisUser["questionsEmail"],
        "pushToken":null,
        "chattingWith":null,
        "uid": widget.thisUser["uid"],
      },  merge: true));
    }


   //Navigator.push(context, MaterialPageRoute(builder: (context) => new AddWebsiteCallPage()));
  }
}

void _buyProduct(ProductDetails prod){
  final PurchaseParam purchaseParam = PurchaseParam(productDetails: prod);
  print("Test");
  _iap.buyNonConsumable(purchaseParam: purchaseParam);
  print("Fatto");
}

void _spendCredits(PurchaseDetails purchase) async{
  setState((){
    // add a function to spend credits
     });

  if( 0 != 14
  // if the variable goes to 0 then the purchase is consumed.
  )
    {
      var res = await _iap.consumePurchase(purchase);
    }
}

void _initialize() async {
  _available = await _iap.isAvailable();

  if(_available){
    await _getProducts();
    await _getPastPurchases();

    _verifyPurchase();

    _subscription = _iap.purchaseUpdatedStream.listen((data) => setState(() {
      print('NEW PURCHASE');
      _purchases.addAll(data);
      _verifyPurchase();
    }));
  }
}

@override
void dispose(){
  _subscription.cancel();
  super.dispose();
}

@override
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      title: Text(_available ? 'Become a PRO'.i18n : 'Not Available'.i18n),
      backgroundColor: Color(0xFF58A9B1),
    ),
    body: Center(
      child: Column(
        children: [
          for (var prod in _products)

          // UI if already purchased
            if (_hasPurchased(prod.id) != null)
              ...[
                Padding(padding: EdgeInsets.only(top: 30),),
                Center(child: Image.asset("assets/diamond.png",width: 70, height: 70,),),
                Center(child: Text(
                  "Already Purchased, \n \n If you have need help contact us at: warmie.trees@gmail.com \n \n Thank you :D".i18n,
                  style: TextStyle(fontSize: 16, color: Color(0xFF58A9B1), fontWeight: FontWeight.w600),
                ),),
              ]

            // UI if NOT purchased
            else ...[
              Padding(padding: EdgeInsets.only(top: 20),),
              new Container(
                  width: 350,
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 40,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 20,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,

                      children: <Widget>[
                        new Text("Include:", style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 18, )),
                        Padding(padding: EdgeInsets.only(top: 20)),
                        new Text(
                            "1) A special place in our PRO page (future)\n2) A personalized page for you \n3) The ability to add website to your page \n4) The ability to add phone to your page \n5) All day support via email and phone \n6) Find more clients \n7) Your listings won’t expire \n8) Cancel subscription at any time \n9) Add Opening Hours (future) \n10) Add details about you (future) \n11)Receive the new feature before the rest".i18n,
                            style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 15)),
                        Padding(padding: EdgeInsets.only(top: 3)),
                      ]
                  )
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
             /* Text(prod.title, style: Theme.of(context).textTheme.headline),
              Text(prod.description),
              Text(prod.price,
                  style: TextStyle(color: Colors.black.withOpacity(0.5), fontSize: 30)),*/
              new GestureDetector(
                onTap: () => _buyProduct(prod),
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  width: 250,
                  padding: EdgeInsets.only(left: 30, right: 30, top: 3, bottom: 3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xFF58A9B1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 10,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  child: Text(
                    "Subscribe now".i18n,
                    style: TextStyle(color: Colors.white, fontSize: 18,fontWeight: FontWeight.w600),
                  ),
                ),),
              /*FlatButton(
                child: Text('Buy It',style: TextStyle(fontSize:20, color: Colors.white)),
                color: Colors.cyan,
                onPressed: () => _buyProduct(prod),
              ),*/
              Padding(padding: EdgeInsets.only(top: 30),),
              new GestureDetector(
                onTap: () => _buyProduct(prod),
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 30, right: 30, top: 3, bottom: 3),
                  child: Text(
                    "Recurring monthly payment, that you can cancel at any time. If you decide to purchase a subscription, the payment will be charged to your iTunes account. The amount will be charged to your account within 24 hours before the end of the current validity period. Once the purchase is made, you can deactivate The automatic renewal at any time from iTunes settings. If you want More information visit our Terms and conditions and the Privacy Policy In profile>settings>others".i18n,
                    style: TextStyle(color: Colors.black.withOpacity(0.3), fontSize: 11,fontWeight: FontWeight.w600),
                  ),
                ),),
            ]
        ],
      ),
    ),
  );
}
}

class SKPaymentQueue {
  static canMakePayments() {}
}