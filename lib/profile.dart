import 'dart:async';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:share/share.dart';
import 'package:warmie/main.dart';
import 'package:i18n_extension/i18n_extension.dart';
import 'package:warmie/purchase.dart';
import 'package:warmie/reviewPage.dart';
import 'main.i18n.dart';
import 'item/item.dart';
import 'dart:io';
import 'main.dart';


/*
UNUSED PACKAGES
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
 */

class ProfilePage extends StatefulWidget {
  ProfilePage({@required this.thisUser, this.showBack});

  var thisUser;
  bool showBack;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  FirebaseUser user;
  Map<dynamic,dynamic> data;
  var onSale;
  var reviews;
  var selled;
  StreamSubscription<List<PurchaseDetails>> _subscription;

  @override
  void initState() {
    final Stream purchaseUpdates =
        InAppPurchaseConnection.instance.purchaseUpdatedStream;
    _subscription = purchaseUpdates.listen((purchases) {
      //_handlePurchaseUpdates(purchases);
    });
    if(widget.thisUser["trees"]==null)(_loadData());
    super.initState();
    _checkLogin();
  }

  void _checkLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }

  void _loadData() async {
    if(widget.thisUser["trees"]==null){
    QuerySnapshot qs = await Firestore.instance.collection('listings').where('seller.uid', isEqualTo: widget.thisUser["uid"]).getDocuments() ;
    data = {"numberOfListings": qs.documents.length};
    onSale = data["numberOfListings"];
    QuerySnapshot qr = await Firestore.instance.collection('reviews').where('seller.uid', isEqualTo: widget.thisUser["uid"]).getDocuments() ;
    data = {"numberOfReviews": qr.documents.length};
    reviews = data["numberOfReviews"];
    if(reviews == null){reviews = 0;}
    QuerySnapshot qso = await Firestore.instance.collection('listings').where('seller.uid', isEqualTo: widget.thisUser["uid"]).where('sold', isEqualTo: "si").getDocuments() ;
    data = {"numberOfSold": qso.documents.length};
    selled = data["numberOfSold"];
    if(selled == null){selled = 0;}
    }

    setState(() {});
  }

  Column _buildButtonColumn(String number, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            number,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: user == null
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: <Widget>[
                Container(
                  color: Color(0xFF58A9B1),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 20,top: 20,bottom: 20),
                  child: SafeArea(
                    child: Row(
                      children: <Widget>[
                        if(widget.thisUser["trees"]!=null)
                        widget.showBack != true
                            ? IconButton(
                                icon: Icon(
                                  Icons.settings,
                                  color: Colors.white,
                                ),
                                onPressed: () async {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => new SettingsPage(thisUser: widget.thisUser, showBack: true,)));
                                },
                              )
                            : Container(),
                        if(widget.thisUser["trees"]==null)
                          widget.showBack == true
                              ? IconButton(
                            padding: EdgeInsets.only(right: 0, left: 0),
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                            ),
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                          )
                              : Container(),
                        CircleAvatar(
                          radius: 32,
                          backgroundImage: NetworkImage(
                            widget.thisUser["avatarURL"].toString(),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                widget.thisUser["name"].toString(),
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                if(widget.thisUser["trees"]!=null)
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50), bottomRight: Radius.circular(50)),
                    color: Color(0xFF58A9B1),
                  ),
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      if(widget.thisUser["trees"] != null)
                      new Text(
                        "You Have Planted:".i18n,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      if(widget.thisUser["trees"] != null)
                      new GestureDetector(
                        onTap: () {
                          Share.share("I've planted " + widget.thisUser["trees"].toStringAsFixed(2) + " trees by selling and buying on Warmie! Join me: http://warmie.it/downloadapp/");
                        },
                        child: Container(
                        padding: EdgeInsets.only(left: 30, right: 30, top: 3, bottom: 3),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                          ),
                          child: Text(
                          widget.thisUser["trees"].toStringAsFixed(2) ?? "0" ,
                          style: TextStyle(color: Color(0xFF58A9B1), fontSize: 18,fontWeight: FontWeight.w600),
                        ),
                      ),),
                    ],
                  ),
                ),
                if(widget.thisUser["prof"]==null)
                  if(widget.thisUser["pro"]!="no")
                  if(widget.thisUser["userSiteUrl"].toString()!= "Your Website..")
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFF58A9B1),
                  ),
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                        new GestureDetector(
                          onTap: () async {
                            String website = widget.thisUser["userSiteUrl"].toString();
                            print(website.substring(0,4));
                            if(website.substring(0,4) == "http"){
                              print(website);
                              launch(website);}
                            else{
                              print(website);
                              launch("http://"+website);
                            }
                            },
                          child: Container(
                            padding: EdgeInsets.only(left: 25, right: 25, top: 3, bottom: 3),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Text(
                              "Website".i18n,
                              style: TextStyle(color: Color(0xFF58A9B1), fontSize: 18,fontWeight: FontWeight.w600),
                            ),
                          ),),
                      if(widget.thisUser["call"].toString()!= "Your Phone Number.." || widget.thisUser["personalPhoneNumber"].toString()!= "Your Salesman Number..")...[
                      new GestureDetector(
                        onTap: () {
                          showCupertinoModalPopup(
                              context: context,
                              builder: (BuildContext context) {
                                return CupertinoActionSheet(
                                  actions: <Widget>[
                                if(widget.thisUser["personalPhoneNumber"].toString()!= "Your Salesman Number..")...[
                                    CupertinoActionSheetAction(
                                      child: Text("Personale: ".i18n + widget.thisUser["personalPhoneNumber"].toString(), style: TextStyle(fontWeight: FontWeight.w600, ),),
                                      onPressed: () async {
                                      },
                                    ),],
                                if(widget.thisUser["call"].toString()!= "Your Phone Number..")...[
                                    CupertinoActionSheetAction(
                                      child: Text("Ufficio: ".i18n + widget.thisUser["call"].toString(), style: TextStyle(fontWeight: FontWeight.w600, ),),
                                      onPressed: () async {
                                      },
                                    ),]
                                  ],
                                  cancelButton: CupertinoActionSheetAction(
                                    child: Text("Cancel".i18n, style: TextStyle(fontWeight: FontWeight.w700,color: Colors.red,),),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                );
                              }
                          );
                          },
                        child: Container(
                          padding: EdgeInsets.only(left: 25, right: 25, top: 3, bottom: 3),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                          ),
                          child: Text(
                            "Call".i18n,
                            style: TextStyle(color: Color(0xFF58A9B1), fontSize: 18,fontWeight: FontWeight.w600),
                          ),
                        ),),],
                      if(widget.thisUser["address"].toString()!= "Your Address..")...[
                        new GestureDetector(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => new OtherInfoPage(thisUser: widget.thisUser, showBack: true,)));
                          },
                          child: Container(
                            padding: EdgeInsets.only(left: 25, right: 25, top: 3, bottom: 3),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Text(
                              "More Info".i18n,
                              style: TextStyle(color: Color(0xFF58A9B1), fontSize: 18,fontWeight: FontWeight.w600),
                            ),
                          ),),]
                    ],
                  ),
                ),
                if(widget.thisUser["trees"]==null)
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50), bottomRight: Radius.circular(50)),
                    color: Color(0xFF58A9B1),
                  ),
                  padding: EdgeInsets.only(bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new GestureDetector(
                        onTap: (){},
                        child: _buildButtonColumn(onSale.toString(), 'On Sale'.i18n),
                      ),
                      new GestureDetector(
                        onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => new ReviewPage(showBack: true)));},
                        child: _buildButtonColumn(selled.toString(), 'Sold'.i18n),
                      ),
                      /*
                      new GestureDetector(
                        onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => new ReviewPage(showBack: true)));},
                        child: _buildButtonColumn(reviews.toString(), 'Reviews'.i18n),
                      ),*/
                    ],
                  ),
                ),


                Expanded(
                  child: Container(
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    child: StreamBuilder(
                        stream: Firestore.instance.collection('listings').where('seller.uid', isEqualTo: widget.thisUser["uid"]).snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
                          if (snapshot.data.documents.length == 0) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Text("There's nothing here! \n Try to sell something".i18n,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      color: Colors.grey,
                                    )),
                              ],
                            );
                          }
                          return GridView.count(
                            crossAxisCount: 3,
                            children: snapshot.data.documents.map<Widget>((DocumentSnapshot v) {
                              bool _display = true;

                              if (_display == true) {
                                if(v["sold"] == "si") {
                                  return Container(
                                    padding: EdgeInsets.all(0),
                                    margin: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey[400],
                                          spreadRadius: 0,
                                          blurRadius: 5,
                                        ),
                                      ],
                                    ),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => new ViewListing(item: v)));
                                      },
                                      child: GridTile(
                                        header: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.black.withOpacity(0.6),
                                            borderRadius: BorderRadius.circular(20),
                                          ),
                                          height: 120,
                                          child: Center(
                                            child: Text("Sold.".i18n, style: TextStyle(color: Colors.white,fontSize: 22, fontWeight: FontWeight.w900),
                                            ),
                                          ),
                                        ),
                                        child: Container(
                                          width: 64,
                                          height: 64,
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius: BorderRadius.circular(20),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[400],
                                                spreadRadius: 0,
                                                blurRadius: 5,
                                              ),
                                            ],
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: CachedNetworkImageProvider(
                                                v["images"][0],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                );}
                                if(v["sold"] == "no") {
                                  return Container(
                                      padding: EdgeInsets.all(0),
                                      margin: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey[400],
                                            spreadRadius: 0,
                                            blurRadius: 5,
                                          ),
                                        ],
                                      ),
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => new ViewListing(item: v)));
                                        },
                                        child: GridTile(
                                          child: Container(
                                            width: 64,
                                            height: 64,
                                            decoration: BoxDecoration(
                                              color: Colors.grey,
                                              borderRadius: BorderRadius.circular(20),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[400],
                                                  spreadRadius: 0,
                                                  blurRadius: 5,
                                                ),
                                              ],
                                              image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image: CachedNetworkImageProvider(
                                                  v["images"][0],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                  );
                                }
                              }
                              return Container();
                            }).toList(),
                          );
                        }),
                  ),
                ),
              ],
            ),
    );
  }
}

//---------------------------------------------------- SETTINGS PAGE -------------------------------------------------


class SettingsPage extends StatefulWidget {
  @override
  SettingsPage({@required this.thisUser, this.showBack});

  var thisUser;
  bool showBack;
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  double trees = 0;
  FirebaseUser user;
    @override
    void initState() {
      super.initState();
      _getTrees();
      _checkLogin();
    }

    void _checkLogin() async {
      user = await FirebaseAuth.instance.currentUser();
      setState(() {});
  }

  void _getTrees() async {
    QuerySnapshot qs = await Firestore.instance.collection('users').getDocuments();

    qs.documents.forEach((v) {
      if (v.data["trees"] != null) {
        trees = trees + v.data["trees"];
      }
    });

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text("Settings".i18n,),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 10),),
            GestureDetector(
              onTap: () async{
                Navigator.push(context, MaterialPageRoute(builder: (context) => new EditProfilePage(thisUser: widget.thisUser, showBack: true,)));
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.photo, color: Color(0xFF7E7E7E)),
                        Text("Edit Profile".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),

            GestureDetector(
              onTap: () async{
                Navigator.push(context, MaterialPageRoute(builder: (context) => new LanguagePage(thisUser: widget.thisUser, showBack: true,)));
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.language, color: Color(0xFF7E7E7E)),
                        Text("Change Language".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                Navigator.push(context, MaterialPageRoute(builder: (context) => new PackagePage(thisUser: widget.thisUser, showBack: true,)));
                },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.airplanemode_active, color: Color(0xFF7E7E7E)),
                        Text("Become a PRO".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                Navigator.push(context, MaterialPageRoute(builder: (context) => new OtherPage(thisUser: widget.thisUser, showBack: true,)));
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.adjust, color: Color(0xFF7E7E7E)),
                        Text("Others".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                await FirebaseAuth.instance.signOut();
                Navigator.push(context, MaterialPageRoute(builder: (context) => new MyHomePage()));
              },
              child: Container(
                padding: EdgeInsets.all(12),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Logout".i18n, textAlign: TextAlign.center, style: TextStyle(decoration: TextDecoration.underline,fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//---------------------------------------------------- LANGUAGE PAGE -------------------------------------------------


class LanguagePage extends StatefulWidget {
  @override
  LanguagePage({@required this.thisUser, this.showBack});

  var thisUser;
  bool showBack;
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  double trees = 0;
  FirebaseUser user;
  @override
  void initState() {
    super.initState();
    _getTrees();
    _checkLogin();
  }

  void _checkLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }

  void _changeLanguageUS() =>
      I18n.of(context).locale = (I18n.localeStr == "en_us") ? null : Locale("en","US");

  void _changeLanguageIT() =>
      I18n.of(context).locale = (I18n.localeStr == "it") ? null : Locale("it");

  void _getTrees() async {
    QuerySnapshot qs = await Firestore.instance.collection('users').getDocuments();

    qs.documents.forEach((v) {
      if (v.data["trees"] != null) {
        trees = trees + v.data["trees"];
      }
    });

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text("Language".i18n,),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 10),),
            GestureDetector(
              onTap: () async{
                _changeLanguageIT();
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.language, color: Color(0xFF7E7E7E)),
                        Text("Clicca per Italiano".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                _changeLanguageUS();
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.language, color: Color(0xFF7E7E7E)),
                        Text("Tap for English".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }}


//---------------------------------------------------- OTHER SETTINGS PAGE -------------------------------------------------


class OtherPage extends StatefulWidget {
  @override
  OtherPage({@required this.thisUser, this.showBack});

  var thisUser;
  bool showBack;
  _OtherPageState createState() => _OtherPageState();
}

class _OtherPageState extends State<OtherPage> {
  double trees = 0;
  FirebaseUser user;
  @override
  void initState() {
    super.initState();
    _getTrees();
    _checkLogin();
  }

  void _checkLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }

  void _getTrees() async {
    QuerySnapshot qs = await Firestore.instance.collection('users').getDocuments();

    qs.documents.forEach((v) {
      if (v.data["trees"] != null) {
        trees = trees + v.data["trees"];
      }
    });

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text("Others".i18n,),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 10),),
            GestureDetector(
              onTap: () async{
                launch('https://www.warmie.it/contact/');
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.contact_mail, color: Color(0xFF7E7E7E)),
                        Text("Contact Us".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                launch('https://www.warmie.it/faq/');
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.book, color: Color(0xFF7E7E7E)),
                        Text("FAQ".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                launch('https://www.warmie.it/privacypolicy/');
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.adjust, color: Color(0xFF7E7E7E)),
                        Text("Privacy Policy".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 15),),
            GestureDetector(
              onTap: () async{
                launch('https://www.warmie.it/terms-of-use/');
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.adjust, color: Color(0xFF7E7E7E)),
                        Text("Terms Of Use".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 20),),
            GestureDetector(
                onTap: () async{},
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Align(
                            child: Text("Warmie app : Version 1.0.21".i18n, style: TextStyle(fontSize:16,fontWeight:FontWeight.w400,color: Colors.black.withOpacity(0.5))),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }}

//---------------------------------------------------- EDIT PROFILE PAGE -------------------------------------------------


class EditProfilePage extends StatefulWidget {
  @override
  EditProfilePage({@required this.thisUser, this.showBack});
  var thisUser;
  bool showBack;
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final nameController = TextEditingController();
  final callController = TextEditingController();
  final websiteController = TextEditingController();
  final companyCapController = TextEditingController();
  final companySloganController = TextEditingController();
  final companyYearsExperienceController = TextEditingController();
  final companyEmployeesNumberController = TextEditingController();
  final personalPhoneController = TextEditingController();
  final companyDescriptionController = TextEditingController();
  final companyAddressController = TextEditingController();
  final shippingCostsController = TextEditingController();
  final instagramLinkController = TextEditingController();
  final facebookLinkController = TextEditingController();
  final whatsappLinkController = TextEditingController();
  final questionsEmailController = TextEditingController();
  double trees = 0;
  FirebaseUser user;

  @override
  void initState() {
    if (widget.thisUser != null) {
      nameController.text = widget.thisUser["name"];
      callController.text = widget.thisUser["call"] != null ? widget.thisUser["call"]:"";
      websiteController.text = widget.thisUser["userSiteUrl"] != null ? widget.thisUser["userSiteUrl"]:"";
      companyCapController.text = widget.thisUser["CAP"] != null ? widget.thisUser["CAP"]:"";
      companySloganController.text = widget.thisUser["companySlogan"] != null ? widget.thisUser["companySlogan"]:"";
      companyYearsExperienceController.text = widget.thisUser["yearsOfExperience"] != null ? widget.thisUser["yearsOfExperience"]:"";
      companyEmployeesNumberController.text = widget.thisUser["numberOfEmployees"] != null ? widget.thisUser["numberOfEmployees"]:"";
      personalPhoneController.text = widget.thisUser["personalPhoneNumber"] != null ? widget.thisUser["personalPhoneNumber"]:"";
      companyDescriptionController.text = widget.thisUser["companyDescription"] != null ? widget.thisUser["companyDescription"]:"";
      companyAddressController.text = widget.thisUser["address"] != null ? widget.thisUser["address"]:"";
      shippingCostsController.text = widget.thisUser["shippingCosts"] != null ? widget.thisUser["shippingCosts"]:"";
      instagramLinkController.text = widget.thisUser["instagramLink"] != null ? widget.thisUser["instagramLink"]:"";
      facebookLinkController.text = widget.thisUser["facebookLink"] != null ? widget.thisUser["facebookLink"]:"";
      whatsappLinkController.text = widget.thisUser["whatsappLink"] != null ? widget.thisUser["whatsappLink"]:"";
      questionsEmailController.text = widget.thisUser["questionsEmail"] != null ? widget.thisUser["questionsEmail"]:"";
    }
    super.initState();
    _getTrees();
    _checkLogin();
  }

  void _checkLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }

  void _getTrees() async {
    QuerySnapshot qs = await Firestore.instance.collection('users')
        .getDocuments();

    qs.documents.forEach((v) {
      if (v.data["trees"] != null) {
        trees = trees + v.data["trees"];
      }
    });

    setState(() {

    });
  }

  void _update() async {
    AlertDialog(title: new Text("Updating profile...".i18n),
      content: new Container(width: 32,
          height: 32,
          child: Center(child: CircularProgressIndicator())
      ),
    );

    //------------ UPDATES THE USER INFO ------------
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    QuerySnapshot qs = await Firestore.instance.collection('users').where('uid', isEqualTo: user.uid).getDocuments();

    await Firestore.instance.collection('users').document(qs.documents.first.documentID).setData({
      "name": nameController.text,
      "call": callController.text,
      "userSiteUrl": websiteController.text,
      "companyDescription": companyDescriptionController.text,
      "address": companyAddressController.text,
      "companySlogan": companySloganController.text,
      "numberOfEmployees": companyEmployeesNumberController.text,
      "yearsOfExperience": companyYearsExperienceController.text,
      "CAP": companyCapController.text,
      "personalPhoneNumber": personalPhoneController.text,
      "shippingCosts": shippingCostsController.text,
      "instagramLink": instagramLinkController.text,
      "facebookLink": facebookLinkController.text,
      "whatsappLink": whatsappLinkController.text,
      "questionsEmail": questionsEmailController.text,
    }, merge: true);

    //------------ UPDATES THE LISTINGS USER INFOS -------------
    QuerySnapshot qr = await Firestore.instance.collection('listings').where('seller.uid', isEqualTo: user.uid).getDocuments();
    qr.documents.forEach((doc) => Firestore.instance.collection('listings').document(doc.documentID).setData({
    "seller": {
      "avatarURL": widget.thisUser["avatarURL"],
      "name": nameController.text,
      "call": callController.text,
      "userSiteUrl": websiteController.text,
      "companyDescription": companyDescriptionController.text,
      "address": companyAddressController.text,
      "companySlogan": companySloganController.text,
      "numberOfEmployees": companyEmployeesNumberController.text,
      "yearsOfExperience": companyYearsExperienceController.text,
      "CAP": companyCapController.text,
      "personalPhoneNumber": personalPhoneController.text,
      "shippingCosts": shippingCostsController.text,
      "instagramLink": instagramLinkController.text,
      "facebookLink": facebookLinkController.text,
      "whatsappLink": whatsappLinkController.text,
      "questionsEmail": questionsEmailController.text,
    }
    },  merge: true));

    //----------- UPDATES THE USER PRO INFO IF THE USER IS A PRO USER ------------
    if(widget.thisUser["prof"] != "no"){
    QuerySnapshot qp = await Firestore.instance.collection('prousers').where('uid', isEqualTo: user.uid).getDocuments();
    qp.documents.forEach((doc) => Firestore.instance.collection('prousers').document(doc.documentID).setData({
    "name": nameController.text,
    "call": callController.text,
    "userSiteUrl": websiteController.text,
    "companyDescription": companyDescriptionController.text,
    "address": companyAddressController.text,
    "companySlogan": companySloganController.text,
    "numberOfEmployees": companyEmployeesNumberController.text,
    "yearsOfExperience": companyYearsExperienceController.text,
    "CAP": companyCapController.text,
    "personalPhoneNumber": personalPhoneController.text,
    "shippingCosts": shippingCostsController.text,
    "instagramLink": instagramLinkController.text,
    "facebookLink": facebookLinkController.text,
    "whatsappLink": whatsappLinkController.text,
    "questionsEmail": questionsEmailController.text,
    },  merge: true));}

    /*QuerySnapshot qr = await Firestore.instance.collection('users').where(
        'uid', isEqualTo: user.uid).getDocuments();

    await Firestore.instance.collection('listings').document(
        widget.thisUser["seller"]["uid"]).setData({

      "name": qs.documents.first.data["name"],
      "call": qs.documents.first.data["call"],
      "uid": widget.thisUser["uid"],
      "userSiteUrl": qs.documents.first.data["userSiteUrl"],

    }, merge: true);*/

    Navigator.pop(context);
    Fluttertoast.showToast(msg: "Profile Updated.".i18n, timeInSecForIos: 3);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text("Edit Profile".i18n,),
        actions: <Widget>[

          FlatButton(
            child: Text("UPDATE".i18n, style: TextStyle(color: Colors.white, fontSize: 16)),
            onPressed: (){_update();},
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 10),),
            GestureDetector(
              onTap: () async{
                File _newImage = await ImagePicker.pickImage(source: ImageSource.gallery);
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return WillPopScope(
                        onWillPop: () async {
                          return false;
                        },
                        child: AlertDialog(
                          title: new Text("Uploading image...".i18n),
                          content: new Container(width: 32, height: 32, child: Center(child: CircularProgressIndicator())),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text("Cancel".i18n),
                              onPressed: () { Navigator.pop(context); },
                            ),
                          ]
                        )
                   );
                  },
                );
                final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child('avatar' + "-" + DateTime.now().millisecondsSinceEpoch.toString() + ".jpg");
                firebaseStorageRef.putFile(_newImage).onComplete.then((v) async {
                  String url = await firebaseStorageRef.getDownloadURL();
                  print(url);

                  QuerySnapshot qs = await Firestore.instance.collection('users').where('uid', isEqualTo: user.uid).getDocuments();
                  await Firestore.instance.collection('users').document(qs.documents.first.documentID).setData({
                    "avatarURL": url,
                  }, merge: true);

                  QuerySnapshot qr = await Firestore.instance.collection('listings').where('seller.uid', isEqualTo: user.uid).getDocuments();
                  qr.documents.forEach((doc) => Firestore.instance.collection('listings').document(doc.documentID).setData({
                    "seller": {
                      "avatarURL": url,
                      }
                  },  merge: true));

                  print("Set profile");
                  widget.thisUser["avatarURL"] = url;

                  Firestore.instance.collection('listings').where('seller.uid', isEqualTo: user.uid).snapshots().forEach((tv) {
                    print("For eaching...");
                    print(tv.documents.length.toString());

                    tv.documents.forEach((DocumentSnapshot ds) async {
                      print("Set p");
                      await Firestore.instance.collection('listings').document(ds.documentID).setData({
                        "seller": {
                          widget.thisUser,
                        }
                      });
                      print("Set!");
                    });
                    print("Done.");
                  });
                });
                Navigator.pop(context);
                Fluttertoast.showToast(msg: "Avatar set.".i18n);
              },
              child: Container(
                padding: EdgeInsets.all(12),
                width: MediaQuery.of(context).size.width ,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 40,
                          backgroundImage: NetworkImage(
                            widget.thisUser["avatarURL"].toString(),
                          ),
                        ),
                        Text(
                          "Change Profile Image".i18n,
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Color(0xFF7E7E7E)),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: "Profile Name".i18n,
                  hintText: "Enter your new name...".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 3)),
            Divider(),
            if(widget.thisUser["prof"].toString() == "no")...[
            GestureDetector(
              onTap: () async{
                Navigator.push(context, MaterialPageRoute(builder: (context) => new PackagePage(thisUser: widget.thisUser, showBack: true,)));
              },
              child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[400],
                      spreadRadius: 0,
                      blurRadius: 3,
                    ),
                  ],
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.airplanemode_active, color: Color(0xFF7E7E7E)),
                        Text("Unlock PRO Features".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
    Padding(padding: EdgeInsets.only(top: 15)),
              Center(child: Image.asset("assets/diamond.png",width: 50, height: 50,),),
              Center(child: Text(
                  "\n Settings > Become a PRO".i18n,
                style: TextStyle(fontSize: 16, color: Color(0xFF58A9B1), fontWeight: FontWeight.w600),
              ),),
            ]
            else if(widget.thisUser["prof"].toString() != "no")...[
    Expanded(
      child: Container(
    child: ListView(
    children: <Widget>[
      Padding(padding: EdgeInsets.only(top: 3)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: websiteController,
                decoration: InputDecoration(
                  labelText: "Website".i18n,
                  hintText: "Enter your website url".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: callController,
                decoration: InputDecoration(
                  labelText: "Office Number".i18n,
                  hintText: "Enter your phone number".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
      Padding(padding: EdgeInsets.only(top: 15)),
      new Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
        child: new TextFormField(
          controller: companyAddressController,
          decoration: InputDecoration(
            labelText: "Address".i18n,
            hintText: "Enter your company address..".i18n,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(
              ),
            ),
          ),
        ),
      ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: companyDescriptionController,
                decoration: InputDecoration(
                  labelText: "Company Description".i18n,
                  hintText: "Enter your company description..".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
      Padding(padding: EdgeInsets.only(top: 15)),
      new Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
        child: new TextFormField(
          controller: questionsEmailController,
          decoration: InputDecoration(
            labelText: "Email for Questions".i18n,
            hintText: "Enter your email..".i18n,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(
              ),
            ),
          ),
        ),
      ),
      Padding(padding: EdgeInsets.only(top: 15)),
      new Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
        child: new TextFormField(
          controller: instagramLinkController,
          decoration: InputDecoration(
            labelText: "Instagram Link".i18n,
            hintText: "Enter your instagram link..".i18n,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(
              ),
            ),
          ),
        ),
      ),
      Padding(padding: EdgeInsets.only(top: 15)),
      new Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
        child: new TextFormField(
          controller: facebookLinkController,
          decoration: InputDecoration(
            labelText: "Facebook Link".i18n,
            hintText: "Enter your facebook link..".i18n,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(
              ),
            ),
          ),
        ),
      ),
      Padding(padding: EdgeInsets.only(top: 15)),
      new Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
        child: new TextFormField(
          controller: whatsappLinkController,
          decoration: InputDecoration(
            labelText: "Whatsapp Link".i18n,
            hintText: "Enter your whatsapp link..".i18n,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(
              ),
            ),
          ),
        ),
      ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: companySloganController,
                inputFormatters: [
                  new LengthLimitingTextInputFormatter(100),
                ],
                decoration: InputDecoration(
                  labelText: "Slogan of the company".i18n,
                  hintText: "Enter max 100 characters..".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: companyEmployeesNumberController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Number of employees".i18n,
                  hintText: "0".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: companyYearsExperienceController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Years of experience".i18n,
                  hintText: "0".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: companyCapController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "CAP".i18n,
                  hintText: "00000".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            new Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
              child: new TextFormField(
                controller: personalPhoneController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Personal Number".i18n,
                  hintText: "Enter salesman number..".i18n,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(
                    ),
                  ),
                ),
              ),
            ),
      Padding(padding: EdgeInsets.only(top: 15)),
      Text( "If you don't change the default value of a field it will not be displayed".i18n,style: TextStyle(color: Colors.black.withOpacity(0.5), fontSize: 12,fontWeight: FontWeight.w600),),
      // ------ In new version of the app we can think of include a settings for shipping costs
      /*Padding(padding: EdgeInsets.only(top: 15)),
      new Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1, top: 10),
        child: new TextFormField(
          controller: shippingCostsController,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            labelText: "Shipping Costs".i18n,
            hintText: "Enter shipping price..".i18n,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(
              ),
            ),
          ),
        ),
      ),*/
      Padding(padding:EdgeInsets.only(top:30, bottom:30)),
           ]))) ]// END OF ELSE
          ],
        ),
      ),
    );
  }}

//---------------------------------------------------- OTHER INFO COMPANY PAGE -------------------------------------------------


class OtherInfoPage extends StatefulWidget {

  @override
  OtherInfoPage({@required this.thisUser, this.showBack});
  var thisUser;
  bool showBack;
  _OtherInfoPageState createState() => _OtherInfoPageState();
}

class _OtherInfoPageState extends State<OtherInfoPage> {
  FirebaseUser user;

  @override
  void initState() {
    super.initState();
    _checkLogin();
  }

  void _checkLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        elevation: 0,
        automaticallyImplyLeading: true,
        title: Text(widget.thisUser["name"],),
      ),
      body: Column(
        children:[ Expanded(
    child: Container(
    child: ListView(
    children: <Widget>[
      Padding(padding: EdgeInsets.only(top:20)),
            //-----------------------------------
            Container(
              padding: EdgeInsets.all(12),
              width: MediaQuery.of(context).size.width ,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // --------------- WHATSAPP LINK ICON -----------
                      if(widget.thisUser["whatsappLink"].toString() != "Your whatsapp link..")...[
                        Padding(padding: EdgeInsets.only(left: 10)),
                        GestureDetector(
                          onTap: () async{
                            String websiteI = widget.thisUser["whatsappLink"].toString();
                            print(websiteI.substring(0,4));
                            if(websiteI.substring(0,4) == "http") {
                              print(websiteI);
                              launch(websiteI);
                            }
                            else{
                              print(websiteI);
                              launch("http://"+websiteI);
                            }
                          },
                          child: CircleAvatar(
                            radius: 25,
                            backgroundImage: AssetImage("assets/whatsappIcon.png"),
                          ),
                        ),],
                      // ---------------- FACEBOOK LINK ICON ------------
                      Padding(padding: EdgeInsets.only(left: 10)),
                      if(widget.thisUser["facebookLink"].toString() != "Your facebook link..")...[
                      GestureDetector(
                        onTap: () async{
                          String websiteF = widget.thisUser["facebookLink"].toString();
                            print(websiteF.substring(0,4));
                            if(websiteF.substring(0,4) == "http") {
                              print(websiteF);
                              launch(websiteF);
                            }
                            else{
                              print(websiteF);
                              launch("http://"+websiteF);
                            }
                        },
                        child: CircleAvatar(
                            radius: 25,
                            backgroundImage: AssetImage("assets/facebookIcon.png"),
                        ),
                      ),
                      //----------------- COMPANY PROFILE IMAGE ---------------
                      Padding(padding: EdgeInsets.only(right: 20)),],
                      CircleAvatar(
                        radius: 40,
                        backgroundImage: NetworkImage(
                          widget.thisUser["avatarURL"].toString(),
                        ),
                      ),

                        //---------------- INSTAGRAM LINK ICON ---------------
                        if(widget.thisUser["instagramLink"].toString() != "Your instagram link..")...[
                      Padding(padding: EdgeInsets.only(left: 20)),
                      GestureDetector(
                        onTap: () async{
                          String websiteI = widget.thisUser["instagramLink"].toString();
                          print(websiteI.substring(0,4));
                          if(websiteI.substring(0,4) == "http") {
                            print(websiteI);
                            launch(websiteI);
                          }
                          else{
                            print(websiteI);
                            launch("http://"+websiteI);
                          }
                        },
                        child: CircleAvatar(
                            radius: 25,
                            backgroundImage: AssetImage("assets/instagramIcon.png"),
                        ),
                      ),],
                      //-------------- EMAIL LINK ICON ------------
                      if(widget.thisUser["questionsEmail"].toString() != "Your questions email..")...[
                        Padding(padding: EdgeInsets.only(left: 10)),
                        GestureDetector(
                          onTap: () async{
                            showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) {
                                  return CupertinoActionSheet(
                                    actions: <Widget>[
                                        CupertinoActionSheetAction(
                                          child: Text("Email: ".i18n + widget.thisUser["questionsEmail"].toString(), style: TextStyle(fontWeight: FontWeight.w600, ),),
                                          onPressed: () async {
                                          },
                                        ),
                                    ],
                                    cancelButton: CupertinoActionSheetAction(
                                      child: Text("Cancel".i18n, style: TextStyle(fontWeight: FontWeight.w700,color: Colors.red,),),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                  );
                                }
                            );
                          },
                          child: CircleAvatar(
                            radius: 25,
                            backgroundImage: AssetImage("assets/emailIcon.png"),
                          ),
                        ),],
                    ],
                  ),
                  //------------------------------------- COMPANY SLOGAN BOX -------------------------------------------
        if(widget.thisUser["companySlogan"].toString() != "Your Company Slogan..")...[
                  Padding(padding: EdgeInsets.only(top: 30),),
                  new Container(
                      width: 350,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 10,
                        bottom: 30,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 20,
                            offset: Offset(0, 0),
                          ),
                        ],
                      ),
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 20)),
                            new Text(widget.thisUser["companySlogan"], style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black.withOpacity(0.5),fontSize: 16)),
                            Padding(padding: EdgeInsets.only(top: 3)),
                          ]
                      )
                  ),
        ],
                  //------------------------------------- COMPANY DESCRIPTION BOX -------------------------------------------
        if(widget.thisUser["companyDescription"].toString() != "Describe Your Company..")...[
                  Padding(padding: EdgeInsets.only(top: 15),),
                  new Container(
                      width: 350,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 30,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 20,
                            offset: Offset(0, 0),
                          ),
                        ],
                      ),
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            new Text("Company Description".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 18, )),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            new Text(widget.thisUser["companyDescription"], style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 16)),
                            Padding(padding: EdgeInsets.only(top: 3)),
                          ]
                      )
                  ),
        ],
                  //------------------------------------- ADDRESS BOX -------------------------------------------
                  Padding(padding: EdgeInsets.only(top: 15),),
                  new Container(
                      width: 350,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 30,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 20,
                            offset: Offset(0, 0),
                          ),
                        ],
                      ),
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            new Text("Address".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 18, )),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            new Text(widget.thisUser["address"], style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 16)),
                      if(widget.thisUser["CAP"].toString() != "00000")...[
                           new Text("CAP: " + widget.thisUser["CAP"], style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 16)),
                            ],Padding(padding: EdgeInsets.only(top: 3)),
                          ]
                      )
                  ),

//------------------------------------- Other Info BOX -------------------------------------------
                  if(widget.thisUser["numberOfEmployees"].toString() != "0" || widget.thisUser["yearsOfExperience"].toString() != "0" )...[
                  Padding(padding: EdgeInsets.only(top: 15),),
                  new Container(
                      width: 350,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 30,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 20,
                            offset: Offset(0, 0),
                          ),
                        ],
                      ),
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            new Text("Other Info".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontWeight: FontWeight.w700, fontSize: 18, )),
                            Padding(padding: EdgeInsets.only(top: 10)),
    if(widget.thisUser["numberOfEmployees"].toString() != "0")...[
                            new Text("Number Of Employees: ".i18n + widget.thisUser["numberOfEmployees"] , style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 16)),],
    if(widget.thisUser["numberOfEmployees"].toString() != "0")...[
                            new Text("Years Of Experience: ".i18n + widget.thisUser["yearsOfExperience"], style: TextStyle(fontWeight: FontWeight.w500, color: Colors.black.withOpacity(0.5),fontSize: 16)),],
                            Padding(padding: EdgeInsets.only(top: 3)),
                          ]
                      )
                  ),
                  Padding(padding: EdgeInsets.only(top: 30),),]
                ],
              ),
            ),

                  ],
                ),),
            ),]),
            );
  }
}