import 'dart:io';

/*

        This is the Ads Part of the app.

        Here you can find all the ads that the app have.

 */

class AdMobService {
  String getAdMobAppId(){
    if(Platform.isIOS) {
      return 'ca-app-pub-1891198946628506~8536443609';
    }
    else if (Platform.isAndroid){
      return 'ca-app-pub-1891198946628506~9511092850';
    }
    return null;
  }

  // -------- BANNER UNDER SEARCH (FULL_BANNER) ---------
  String getBannerAdUnderSearchId(){
    if(Platform.isIOS) {
      return 'ca-app-pub-1891198946628506/8352836115';
    }
    else if (Platform.isAndroid){
      print("sono sotto la search");
      return 'ca-app-pub-1891198946628506/2713496629';
    }
    return null;
  }

  // -------- FIRST BANNER AT THE TOP OF A LISTING (LEADERBOARD) -------
  String getBannerAdId1(){
    if(Platform.isIOS) {
      return 'ca-app-pub-1891198946628506/7705705523';
    }
    else if (Platform.isAndroid){
      return 'ca-app-pub-1891198946628506/4520304835';
    }
    return null;
  }

  // -------- SECOND BANNER AT THE BOTTOM OF A LISTING (FULL_BANNER) -------
  String getBannerAdId2(){
    if(Platform.isIOS) {
      return 'ca-app-pub-1891198946628506/4988306178';
    }
    else if (Platform.isAndroid){
      print("i'm here");
      return 'ca-app-pub-1891198946628506/7557795040';
    }
    return null;
  }
}