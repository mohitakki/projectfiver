import 'dart:ffi';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:auto_localization/auto_localization.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'home.dart';
import 'main.i18n.dart';
import 'main.dart';
import 'item/create.dart';
import 'item/categories.dart';
import 'item/item.dart';
import 'conversation.dart';
import 'profile.dart';
import 'admin.dart';
import 'news.dart';
import 'admob_service.dart';
import 'search.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

//----------------------------------------------------- Project Started On 30/9/2019 ------------------------------------------------

/*

          When I started to think of this project, it was 30/1/2019, we want to create this app and release it on 30/11/2019
          But due to problems with the ios version of the app we had to move to release to 30/4/2020
          I've got a lot of people helping me on this project, from all the parts of the world.
          Italy, London, India, Pachistan, France, USA, China.

          And i want to give a huge thanks to all of them, without their help and your help, this project would not have seen the light
          So thank you. You are helping me create a better world.

          ~ Christian Braglia 2020/3/14

*/
//-------------------- if you are reading this, then my program is probably a success ~ Christian Braglia 2020/3/14 -----------------

/*
      When I wrote this, only God and I
      Understood what i was doing
      Now, God only knows.
 */
/*
      There are 2 rules in life:
      - Number 1 – Never quit.
      - Number 2 – Never forget rule number 1.
 */
/*
      If it’s your job to eat a frog,
      it’s best to do it first thing in the morning.
      And If it’s your job to eat two frogs,
      it’s best to eat the biggest one first.
 */

//----------------------------------------------------------------------------------------------------------------------------------

class FeedPage extends StatefulWidget {
  FeedPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  int _selectedIndex = 0;
  final ams = AdMobService(); //this is used for the ads in the app
  FirebaseUser user;
  double trees = 0;
  DocumentSnapshot tuser;
  List<Widget> screen = [];

  @override
  void initState() {
    super.initState();
    Admob.initialize(ams.getAdMobAppId());
    _getLogin();
    _getTrees();
    registerNotification();
  }

  void _getTrees() async {
    QuerySnapshot qs = await Firestore.instance.collection('users').getDocuments();

    qs.documents.forEach((v) {
      if (v.data["trees"] != null) {
        trees = trees + v.data["trees"];
      }
    });

    setState(() {

    });
  }

  //--------------------------------------------------- ADD TREES FUNCTION -----------------------------------------------

  void _addTree() async {
    if (tuser.data["trees"] == null) {
      await Firestore.instance.collection('users').document(tuser.documentID).setData({
        "trees": 0.01,
      }, merge: true);
      _getLogin();
    } else {
      await Firestore.instance.collection('users').document(tuser.documentID).setData({
        "trees": tuser.data["trees"] + 0.01,
      }, merge: true);
    }
    _getLogin();
  }

  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  void registerNotification() {
    firebaseMessaging.requestNotificationPermissions();

    firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
      print('onMessage: $message');

      return;
    }, onResume: (Map<String, dynamic> message) {
      print('onResume: $message');
      return;
    }, onLaunch: (Map<String, dynamic> message) {
      print('onLaunch: $message');
      return;
    });

    firebaseMessaging.getToken().then((token) {
      print('token: $token');
      Firestore.instance
          .collection('users')
          .document('uid')
          .updateData({'pushToken': token});
    }).catchError((err) {
      print("nothing");
    });
  }

  void _getLogin() async {
    user = await FirebaseAuth.instance.currentUser();
    print(user.uid);
    QuerySnapshot _qs = await Firestore.instance.collection('users').where('uid', isEqualTo: user.uid).getDocuments();
    tuser = _qs.documents.first;

    List<List<String>> categories = [
      ["Clothing & Accessories".i18n, "tshirt.png"],
      ["Electronics".i18n, "smartphone.png"],
      ["Cars & Accessories".i18n, "cars.png"],
      ["Movie, Books and Music".i18n, "books.png"],
      ["Property On Sale".i18n, "house.png"],
     // ["Property For Rent", "house.png"],
      ["Services".i18n, "services.png"],
     // ["See all Categories", "others.png"],
    ];
    //-------------------------------------------------------------- ALL THE SCREENS --------------------------------------------------------------
    screen = [
      HomePage(),
      SearchPage(),

      //------------------------------------------------------------- SELL TAB -----------------------------------------------------------

      Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Sell an Item & Help the World".i18n),
          backgroundColor: Color(0xFF58A9B1),
          automaticallyImplyLeading: false,
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton.extended(
          heroTag: "btn1",
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => new AllCategoriesPage())).then((k) {
          if (k != null) {
          Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: k)));
          }
          });
         },
          label: Text('See All The Categories'.i18n),
          elevation: 5,
          backgroundColor: Color(0xFF58A9B1),
         ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: ListView(
          children: <Widget>[

            Padding(padding: EdgeInsets.only(top: 10)),
            Center(child: Text(trees.toStringAsFixed(0), style: TextStyle(color: Color(0xFF58A9B1), fontSize: 32, fontWeight: FontWeight.w800,)),),

            //-------------------------------------------------------- TREES TEXT -----------------------------------------------------
            Center(child: Text("Trees planted by Warmie".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontSize: 18, fontWeight: FontWeight.w700,)),),
            //-------------------------------------------------- LINKS TO 'WARMIE.IT' ---------------------------------------------------
            Padding(padding: EdgeInsets.only(top:5)),
            SizedBox(height: 5.0),
            Container(
             padding: EdgeInsets.all(30.0),
             width: MediaQuery.of(context).size.width - 30.0,
             height: MediaQuery.of(context).size.width - 10.0,
              child: GridView.count(
                  crossAxisCount: 3,
                primary: false,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 15.0,
                childAspectRatio: 0.7,
                children: <Widget>[
                  _buildcard('Clothes'.i18n, 'assets/tshirt.png',context,0),
                  _buildcard('Electronics'.i18n, 'assets/smartphone.png',context,1),
                  _buildcard('Cars'.i18n, 'assets/cars.png',context,2),
                  _buildcard('Books'.i18n, 'assets/books.png',context,3),
                  _buildcard('House'.i18n, 'assets/house.png',context,4),
                  _buildcard('Services'.i18n, 'assets/services.png',context,5),
                ],
              ),
            )
          ],
          /*child: GridView.count(
            crossAxisCount: 3,
            children: categories.map<Widget>((v) {
              return FlatButton(
                child: Container(
                  padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(900),
              color: Color(0xFF58A9B1),),
              child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Image.asset(
                        "assets/" + v[1],
                        fit: BoxFit.fill,
                        width: 40,
                        height: 40,
                      ),
                      Padding(padding: EdgeInsets.only(top: 10)),
                      /*new Text(
                          v[0],
                          style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
                          textAlign: TextAlign.center,
                      ),*/
                    ],
                  ),
                ),
                onPressed: () {
                  if (v[0] != "See all Categories") {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: v[0])));
                  } else {

                  }
                },
              );
            }).toList(),
          ),*/
        ),
      ),

      //-------------------------------------------------------------- CHAT TAB --------------------------------------------------------------
      Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Chats".i18n),
          backgroundColor: Color(0xFF58A9B1),
          automaticallyImplyLeading: false,
          centerTitle: true,
        ),

        //------------------------ IF NO MESSAGE THIS IS THE PART SHOWED -------------------------------
        body: Center(
          child: StreamBuilder(
              stream: Firestore.instance.collection('inbox').where('users', arrayContains: user.uid).snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
                if (snapshot.data.documents.length == 0) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text("No messages yet?".i18n,
                          style: TextStyle(
                              color: Color(0xFF2C666E),
                              fontSize: 28,
                              fontWeight: FontWeight.w600
                          )),
                      Padding(padding: EdgeInsets.only(top:20),),
                      new Image.asset(
                        "assets/nomsg.png",
                        width: MediaQuery.of(context).size.width * 0.4,
                      ),
                      new Padding(padding: EdgeInsets.only(top: 20)),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: new Text(
                          "We'll keep messages for any item you're trying to buy/sell in here".i18n,
                          style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black.withOpacity(0.5),fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  );
                }

                //------------------------------------ IF THERE ARE SOME MESSAGES, THIS IS THE PART SHOWED ---------------------------
                return ListView(
                  children: snapshot.data.documents.map<Widget>((DocumentSnapshot v) {
                    return ListTile(
                      //------------------------------ Item image --------------------------------
                      leading: Container(
                        width: 64,
                        height: 64,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: CachedNetworkImageProvider(v["item"]["image"] //
                            ),
                          ),
                        ),
                      ),
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Divider(thickness: 2,),
                          //------------------------------------- Username --------------------------
                          Text(v["users"][1] == user.uid ? v["userData"][1]["name"] : v["userData"][1]["name"], style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.left),
                          Text(
                            v["item"]["title"],
                            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 16, color: Colors.black),
                            textAlign: TextAlign.left,
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 0)),
                        ],
                      ),
                      //---------------------------------------- Last message --------------------------
                      subtitle: Text(
                        v["messages"].last["body"],
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      /*trailing: CircleAvatar(
                        backgroundImage: CachedNetworkImageProvider(
                          v["users"][0] == user.uid ? v["userData"][1]["avatarURL"] : v["userData"][0]["avatarURL"],
                       ),
                      ),*/
                      //--------------------------- On Tap open chat ----------------------
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new Conversation(
                                    title: v["users"][0] == user.uid ? v["userData"][1]["name"] : v["userData"][0]["name"],
                                    avatarURL: v["users"][1] == user.uid ? v["userData"][0]["avatarURL"] : v["userData"][1]["avatarURL"],
                                    documentID: v.reference.documentID,
                                    item: {"title": v["item"]["title"], "image": v["item"]["image"], "price": v["item"]["price"]},
                                    us: v["users"][0] == user.uid ? 0 : 1))).then((a) {
                          if (a == "delete") {
                            Firestore.instance.collection('inbox').document(v.reference.documentID).delete();
                          }
                        });
                      },
                    );
                  }).toList(),
                );
              }),
        ),
      ),

      ProfilePage(
        thisUser: tuser.data,
        showBack: false,
      ),
    ];

    setState(() {});
  }


  //------------------------------------------------- BOTTOM NAVIGATION BAR -------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        floatingActionButton: tuser != null && tuser.data["admin"] == true ? FloatingActionButton(child: Icon(Icons.security), onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => new AdminPage()));
        },) : Container(),
        backgroundColor: Colors.white,
        bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            selectedIconTheme: IconThemeData(
              color: Color(0xFF58A9B1),
            ),
            iconSize: 32,
            unselectedItemColor: Colors.black.withOpacity(0.5),
            showUnselectedLabels: true,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('Home'.i18n, style: TextStyle(color: Colors.grey)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.search),
                title: Text('Search'.i18n, style: TextStyle(color: Colors.grey)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.add_circle_outline),
                title: Text('Sell'.i18n, style: TextStyle(color: Colors.grey)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.chat_bubble_outline),
                title: Text('Chat'.i18n, style: TextStyle(color: Colors.grey)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person_outline),
                title: Text('Profile'.i18n, style: TextStyle(color: Colors.grey)),
              ),
            ],
            currentIndex: _selectedIndex,
            onTap: (v) {
              if (v == 0) {
                _addTree();
              } else if (v == 2) {
                _addTree();
              }
              setState(() {
                _selectedIndex = v;
              });
            }),
        body: Center(
          child: user == null || tuser == null ? CircularProgressIndicator() : screen[_selectedIndex],
        ),
      ),
    );
  }
}

//------------------------------------------------------- ADVANCED RESEARCH PAGE --------------------------------------------------

class AdvancedResearchPage extends StatefulWidget {
  @override
  _AdvancedResearchPageState createState() => _AdvancedResearchPageState();
}

class _AdvancedResearchPageState extends State<AdvancedResearchPage> {
  String category = "All the categories".i18n;
  String location = "All the world".i18n;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF58A9B1),
        automaticallyImplyLeading: true,
        title: Text("Advanced Research".i18n),
      ),
      bottomNavigationBar: Container(
        height: 80,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(40.0),
            topRight: const Radius.circular(40.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[300],
              spreadRadius: 4,
              blurRadius: 8,
            ),
          ],
        ),
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            new FlatButton(
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: Color(0xFF58A9B1),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(
                  "Apply".i18n,
                  style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () async {
                Navigator.pop(context, {"category": category.i18n, "location": location.i18n});
                // Navigator.push(context, MaterialPageRoute(builder: (context) => new Conversation(title: v["users"][0] == user.uid ? v["userData"][0]["name"] : v["userData"][1]["name"], avatarURL:  v["users"][0] == user.uid ? v["userData"][0]["avatarURL"] : v["userData"][1]["avatarURL"], documentID: ds.documentID, item: {"title": v["item"]["title"], "image": v["item"]["image"], "price": v["item"]["price"]}, us: v["users"][0] == user.uid ? 0 : 1)));
              },
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new AllLocationPage())).then((v) {
                  if (v != null) {
                    setState(() {
                      location = v;
                    });
                  }
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text("Location".i18n, style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w800, fontSize: 24)),
                    Padding(padding: EdgeInsets.only(top: 3)),
                    Divider(),
                    Padding(padding: EdgeInsets.only(top: 0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(location, textAlign: TextAlign.start, style: TextStyle(color: Color(0xFF7E7E7E))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 3)),
                  ],
                ),
              ),
            ),
          Divider(),
          Padding(padding: EdgeInsets.only(top: 20)),
            GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new AllCategoriesPage())).then((v) {
                  if (v != null) {
                    setState(() {
                      category = v;
                    });
                  }
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text("Category".i18n, style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w800, fontSize: 24)),
                    Padding(padding: EdgeInsets.only(top: 3)),
                    Divider(),
                    Padding(padding: EdgeInsets.only(top: 0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(category, textAlign: TextAlign.start, style: TextStyle(color: Color(0xFF7E7E7E))),
                        Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 3)),
                    Divider(),
                    Padding(padding: EdgeInsets.only(top: 0)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


//----------------------------------------------------------------------------------------------------

//----------------------------------- Cards of the sell page -----------------------------------------

//----------------------------------------------------------------------------------------------------

      Widget _buildcard(String category, String imagePath, context, int idCategory){
        return Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 5.0, left: 5.0, right: 5.0),
          child: InkWell(
            onTap: () {
              if(idCategory==0)
                {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: "Clothing & Accessories".i18n, type: "Select".i18n)));
                }
              if(idCategory==1)
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: "Electronics".i18n,type: "Select".i18n)));
              }
              if(idCategory==2)
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: "Vehicles & Accessories".i18n,type: "Select".i18n)));
              }
              if(idCategory==3)
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: "Movie, Books and Music".i18n,type: "Select".i18n)));
              }
              if(idCategory==4)
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: "Property On Sale".i18n,type: "Select".i18n)));
              }
              if(idCategory==5)
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => new CreateListing(category: "Services".i18n,type: "Select".i18n)));
              }
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 3.0,
                    blurRadius: 5.0,
                  )
                ],
                  color: Colors.white,
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                      ]
                    )
                  ),
                  //-------------------------------- Category Image -------------------------------
                  Hero(
                    tag: imagePath,
                    child: Container(
                        height: 75.0,
                        width: 75.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(imagePath),
                                fit: BoxFit.contain
                            )
                        )
                  )
                  ),
                  //------------------------------------- Text ----------------------------------
                  SizedBox(height: 7.0),
                  Text(category,
                      style: TextStyle(
                        color: Colors.black.withOpacity(0.5),
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                      )),
                ]
              ),
            )
          ),
        );
      }