import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:url_launcher/url_launcher.dart';
import 'main.i18n.dart';


class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  double trees = 0;

  @override
  void initState() {
    super.initState();
    _getTrees();
  }

  void _getTrees() async {
    QuerySnapshot qs = await Firestore.instance.collection('users').getDocuments();

    qs.documents.forEach((v) {
      if (v.data["trees"] != null) {
        trees = trees + v.data["trees"];
      }
    });

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: <Widget>[
        Container(
          color: Color(0xFF58A9B1),
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(10),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("News".i18n, style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600, fontSize: 18,),),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(5),
          color: Color(0xFF58A9B1),
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "About every 45 searches we plant a tree".i18n,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ), /* Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("Order", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
                    Icon(Icons.keyboard_arrow_down, color: Colors.white,),
                  ],
                ),*/
            ],
          ),
        ),
        //--------------------------------------------------- TOTAL NUMBER OF TREES ------------------------------------------------
        Padding(padding: EdgeInsets.only(top: 10)),
        new Text(trees.toStringAsFixed(0), style: TextStyle(color: Color(0xFF58A9B1), fontSize: 32, fontWeight: FontWeight.w800,)),
        Padding(padding: EdgeInsets.only(top: 5)),
        //-------------------------------------------------------- TREES TEXT -----------------------------------------------------
        new Text("Trees planted by Warmie".i18n, style: TextStyle(color: Color(0xFF58A9B1), fontSize: 18, fontWeight: FontWeight.w700,)),
        //-------------------------------------------------- LINKS TO 'WARMIE.IT' ---------------------------------------------------
        Padding(padding: EdgeInsets.only(top: 30)),
        GestureDetector(
          onTap: () async{
            launch('https://www.patreon.com/warmie');
          },
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  spreadRadius: 0,
                  blurRadius: 3,
                ),
              ],
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width * 0.9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.adjust, color: Color(0xFF7E7E7E)),
                    Text("Support The App".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                    Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 20)),
        GestureDetector(
          onTap: () async{
            launch('https://warmie.it/financialreports/');
          },
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  spreadRadius: 0,
                  blurRadius: 3,
                ),
              ],
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width * 0.9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.account_balance_wallet, color: Color(0xFF7E7E7E)),
                    Text("Financial Reports".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                    Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 20)),
        GestureDetector(
          onTap: () async{
            launch('https://www.warmie.it/projects/');
          },
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  spreadRadius: 0,
                  blurRadius: 3,
                ),
              ],
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width * 0.9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.assessment, color: Color(0xFF7E7E7E)),
                    Text("Projects".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                    Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 20)),
        GestureDetector(
          onTap: () async{
            launch('https://t.me/warmie_official');
          },
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  spreadRadius: 0,
                  blurRadius: 3,
                ),
              ],
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width * 0.9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.assistant_photo, color: Color(0xFF7E7E7E)),
                    Text("Telegram Group".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                    Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 20)),
        GestureDetector(
          onTap: () async{
            launch('https://www.nytimes.com/interactive/2019/12/02/climate/air-pollution-compare-ar-ul.html');
          },
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  spreadRadius: 0,
                  blurRadius: 3,
                ),
              ],
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width * 0.9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.ac_unit, color: Color(0xFF7E7E7E)),
                    Text("See how is the air in your city".i18n, textAlign: TextAlign.center, style: TextStyle(fontSize:16,fontWeight:FontWeight.w600,color: Colors.black.withOpacity(0.5))),
                    Icon(Icons.arrow_forward_ios, color: Color(0xFF7E7E7E)),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}