import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:auto_localization/auto_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/services.dart';
import 'main.i18n.dart';
import 'profile.dart';
import 'onboarding/introduction.dart';
import 'feed.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {

  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool _initialized = false;
  FirebaseMessaging messaging = FirebaseMessaging();

  @override
  void initState() {

    messaging.configure(
      // ignore: missing_return
      onLaunch: (Map<String,dynamic> event){},
      // ignore: missing_return
      onMessage: (Map<String,dynamic> event){},
      // ignore: missing_return
      onResume: (Map<String,dynamic> event){},

    );
    messaging.requestNotificationPermissions(IosNotificationSettings(
      sound: true,
      badge: true,
      alert: true
    ));
    messaging.getToken().then((msg){});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
return I18n(
            child: MaterialApp(
              initialRoute: '/',
              title: 'Warmie',
              theme: ThemeData(
                  brightness: Brightness.light,
                  primarySwatch: Colors.blueGrey,
                  fontFamily: 'Poppins'
              ),

              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],

              supportedLocales: [
                Locale('en','US'),
                Locale('it'),
              ],
              home: I18n(
                initialLocale: Locale("it"),
                child: MyHomePage(title: 'MyHomePage'),
              ),
              debugShowCheckedModeBanner: false,
            )
            );
          }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _loaded = false;

  @override
  void initState() {
    super.initState();
    _checkLogin();
  }

  void _checkLogin() async {
    FirebaseUser _user = await FirebaseAuth.instance.currentUser();

    if (_user != null) {

      Navigator.push(context, MaterialPageRoute(builder: (context) => new FeedPage()));
    } else {
      setState(() {
        _loaded = true;
      });
    }
  }
//------------------------ app --------------------------------------------
  @override
  Widget build(BuildContext context) {
      return Scaffold(
        body: Center(
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height,
            decoration: new BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.center,
                colors: [
                  Color(0xFF58A8B1),
                  Color(0xFF2B5559),
                ],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/onlyLogo.png",
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * 0.8,
                ),
                Padding(padding: EdgeInsets.only(top: 35)),
                _loaded == false
                    ? CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),)
                    : FlatButton(
                  child: FlatButton(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            blurRadius: 5.0,
                            // has the effect of softening the shadow
                            spreadRadius: 0,
                            // has the effect of extending the shadow
                            offset: Offset(
                              0.0, // horizontal, move right 10
                              0.0, // vertical, move down 10
                            ),
                          )
                        ],
                        borderRadius: BorderRadius.circular(30),
                        color: Color(0xFFE3E8EB),
                      ),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * 0.8,
                      child: Text("Get Started".i18n, style: TextStyle(
                          fontSize: 20.0,
                          color: Color(0xFF2C666E),
                          fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,),),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => new OnboardingPage(title: "test",)));
                    },
                  ),
                ),
              ],
            ),
          ),
        )
      );
    }
}

// THIS IS THE FIRST PAGE OF WARMIE (SPLASHSCREEN)