package com.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

//import javax.inject.Inject;


public class MyFcmListenerService extends FirebaseMessagingService {

//    @Inject
//    NotificationReceivedPresenter mPresenter;
//
//    @Override
//    public void onMessageReceived(RemoteMessage pRemoteMessage) {
//
//        initialize();
//
//        Map<String, String> data = pRemoteMessage.getData();
//        Logger.d("MyGcmListenerService_onMessageReceived", "data:" + data.toString());
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "getInstanceId failed", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        String token = task.getResult().getToken();
//
//                        // Log and toast
//                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
//                    }
//                });
//        String title = data.get("title");
//        String description = data.get("description");
//        description = description != null ? description : "";
//
//        if (title != null) {
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.drawable.ic_notification)
//                    .setContentTitle(title)
//                    .setContentText(description)
//                    .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat
//                            .DEFAULT_VIBRATE)
//                    .setLights(Color.GREEN, 2000, 3000)
//                    .setAutoCancel(true)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(description));
//
//            Intent resultIntent = new Intent(this, SplashScreenActivity.class);
//
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//            stackBuilder.addParentStack(SplashScreenActivity.class);
//            stackBuilder.addNextIntent(resultIntent);
//            PendingIntent resultPendingIntent =
//                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//            builder.setContentIntent(resultPendingIntent);
//            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context
//                    .NOTIFICATION_SERVICE);
//            mNotificationManager.notify(2390, builder.build());
//
//            saveNotificationToDB(title, description);
//        }
//    }
//
//    @Override
//    public void onNewToken(String token) {
//        Log.d(TAG, "New token: " + token);
//        // TODO: Implement this method to send any registration to your app's servers.
//        sendRegistrationToServer(token);
//    }
//
//    private void initialize() {
//        DaggerDataComponent.builder()
//                .activityModule(new ActivityModule())
//                .applicationComponent(DaggerApplicationComponent.builder()
//                        .applicationModule(new ApplicationModule((MyApplication) this
//                                .getApplicationContext()))
//                        .build())
//                .dataModule(new DataModule())
//                .build()
//                .inject(this);
//    }
//
//    private void saveNotificationToDB(String title, String description) {
//        UseCaseData useCaseData = new UseCaseData();
//
//        Notification notification = new Notification();
//        notification.setTitle(title);
//        notification.setDescription(description);
//        notification.setCreatedAt(System.currentTimeMillis() / 1000);
//        notification.setUpdatedAt(System.currentTimeMillis() / 1000);
//        useCaseData.putSerializable(UseCaseData.SUBMISSION_DATA, notification);
//        mPresenter.initialize(useCaseData);
//    }
}